#!/bin/python

import sys


def main():
    if sys.argv[1] == 'export':
        export()
    else:
        default()


def default():
    with open("toolbox.xml", "r") as fp:
        file = open("toolbox.js", "w")
        file.write('var toolbox = "";\n')
        for line in fp:
            line = line.replace('\'', '\\\'')
            file.write('toolbox += '+'\''+line.replace('\n', "")+'\';\n')
        file.close()

def export():
    with open("toolbox.xml", "r") as fp:
        file = open("toolbox.js", "w")
        file.write('export const toolbox = \n`\n')
        for line in fp:
            file.write(line)
        file.write('`')
        file.close()
    file = open("toolbox.d.ts", "w")
    file.write('export const toolbox: string')
    file.close()


main()
