/*
 * gpio_mode
 */
Blockly.Blocks['gpio_mode'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Modo Pin");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["Entrada", "gpio.INPUT"], ["Salida", "gpio.OUTPUT"], ["Interrupción", "gpio.INT"]]), "mode");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};


Blockly.Lua['gpio_mode'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_mode = block.getFieldValue('mode');
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.mode('+value_pin+', '+dropdown_mode+')\n';
  return code;
};

/*
 * gpio_write
 */
Blockly.Blocks['gpio_write'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Escribir Pin");
    this.appendDummyInput()
        .appendField("a")
        .appendField(new Blockly.FieldDropdown([["Alto", "gpio.HIGH"], ["Bajo", "gpio.LOW"], ["1", "1"], ["0", "0"]]), "write");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_write'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_write = block.getFieldValue('write');
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.write('+value_pin+', '+dropdown_write+')\n';
  return code;
};

/*
 * gpio_write_variable
 */
Blockly.Blocks['gpio_write_variable'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Escribir")
        .appendField(new Blockly.FieldVariable("item"), "object")
        .appendField("a")
        .appendField(new Blockly.FieldDropdown([["Alto", "gpio.HIGH"], ["Bajo", "gpio.LOW"], ["1", "1"], ["0", "0"]]), "write");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_write_variable'] = function(block) {
  var variable_object = Blockly.Lua.variableDB_.getName(block.getFieldValue('object'), Blockly.Variables.NAME_TYPE);
  var dropdown_write = block.getFieldValue('write');
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.write('+variable_object+', '+dropdown_write+')\n';
  return code;
};

/*
 * gpio_read
 */
Blockly.Blocks['gpio_read'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("Leer Pin");
    this.setInputsInline(true);
    this.setOutput(true, "Number");
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_read'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.read('+value_pin+')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

/*
 * gpio_read_variable
 */
Blockly.Blocks['gpio_read_variable'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Leer")
        .appendField(new Blockly.FieldVariable("item"), "Object");
    this.setInputsInline(true);
    this.setOutput(true, "Number");
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_read_variable'] = function(block) {
  var variable_object = Blockly.Lua.variableDB_.getName(block.getFieldValue('Object'), Blockly.Variables.NAME_TYPE);
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.read('+variable_object+')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

Blockly.Blocks['gpio_trig'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField("En pin");
    this.appendDummyInput()
        .appendField("cuando")
        .appendField(new Blockly.FieldDropdown([["sube", "up"], ["baja", "down"], ["ambos", "both"], ["nivel bajo", "low"], ["nivel alto", "high"]]), "type");
    this.appendStatementInput("callback")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_type = block.getFieldValue('type');
  var statements_callback = Blockly.Lua.statementToCode(block, 'callback');
  // TODO: Assemble Lua into code variable.
  var code = `gpio.trig(${value_pin}, "${dropdown_type}", function(level, when)
  ${statements_callback} end)
`;
  return code;
};

// gpio_trig_var
Blockly.Blocks['gpio_trig_var'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("En")
        .appendField(new Blockly.FieldVariable("item"), "pin");
    this.appendDummyInput()
        .appendField("cuando")
        .appendField(new Blockly.FieldDropdown([["sube", "up"], ["baja", "down"], ["ambos", "both"], ["nivel bajo", "low"], ["nivel alto", "high"]]), "type");
    this.appendStatementInput("callback")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_var'] = function(block) {
  var variable_pin = Blockly.Lua.variableDB_.getName(block.getFieldValue('pin'), Blockly.Variables.NAME_TYPE);
  var dropdown_type = block.getFieldValue('type');
  var statements_callback = Blockly.Lua.statementToCode(block, 'callback');
  // TODO: Assemble Lua into code variable.
  var code = `gpio.trig(${variable_pin}, "${dropdown_type}", function(level, when)
  ${statements_callback} end)
`;
  return code;
};

// trig level
Blockly.Blocks['gpio_trig_level'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("int.nivel");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_level'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = 'level';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

// trig when
Blockly.Blocks['gpio_trig_when'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("int.cuando");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_when'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = 'when';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

// comparator trig
Blockly.Blocks['gpio_trig_compare'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("tipo")
        .appendField(new Blockly.FieldDropdown([["alto", "high"], ["bajo", "low"]]), "level");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(210);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['gpio_trig_compare'] = function(block) {
  var dropdown_level = block.getFieldValue('level');
  // TODO: Assemble Lua into code variable.
  var code = `"${dropdown_level}"`;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

/*
 * adc_read
 */
 Blockly.Blocks['adc_read'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("leer ADC");
    this.setOutput(true, null);
    this.setColour(120);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['adc_read'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = 'adc.read(0)';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};
