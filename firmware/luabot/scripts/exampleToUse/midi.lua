--nodemcu como entrada midi para un pc
--use miditty para que sea reconicido como tal

gpio.mode(0, gpio.INPUT)
gpio.mode(1, gpio.INPUT)

local ON = 144
local OFF = 128

note = 0
state = 0

mi_alarma = tmr.create()
mi_alarma:register( 50, tmr.ALARM_AUTO, function()
  note = (127*adc.read(0))/1023
  if gpio.read(1) == gpio.HIGH then
    state = ON
  else
    state = OFF
  end
  uart.write(0, string.char(state), string.char(note), string.char(90))
  if gpio.read(0) == HIGH then
    mi_alarma:stop()
  end
end)

mi_alarma:start()
