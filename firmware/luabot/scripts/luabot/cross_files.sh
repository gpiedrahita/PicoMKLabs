#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	crosscompilador de archivos
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Monday 20 July 2020
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

PATH_LUACROSS=/home/johnny/projects/catalejo-bit/luabot/software/embedded/nodemcu/firmware/manual/nodemcu-firmware

LUACROSS=$PATH_LUACROSS/luac.cross

$LUACROSS -o lc/init.lc lua/init.lua
$LUACROSS -o lc/server.lc lua/server.lua
$LUACROSS -o lc/wifi.lc lua/wifi.lua
$LUACROSS -o lc/apst.lc lua/apst.lua
$LUACROSS -o lc/ds18b20.lc lua/ds18b20.lua
# $LUACROSS -o lc/ds18b20.lc -s lua/ds18b20.lua
# $LUACROSS ds18b20.lua

# $LUACROSS -o cross_files/init.lua init.lua server.lua wifi.lua apst.lua

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ];
then
  printf "Help for this command cross_files\n"
  printf "\tcross_files Command options\n"
  printf "\t[Commands]\n"
  printf "\t\tcommand1\tbrief1\n"
  printf "\t\tcommand2\tbrief2\n"
  printf "\t\tcommand3\tbrief3\n"
  printf "\t\t-h,--help\tHelp\n"
  printf "\nRegards Johnny.\n"
elif [ "$1" = "command1" ];
then
  echo "pass"
elif [ "$1" = "command2" ];
then
  echo "pass"
elif [ "$1" = "command3" ];
then
  echo "pass"
fi

