package catalejoEditor;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blocklywebview.R;

import java.util.ArrayList;
import java.util.List;

public class BitacoraListaAdapter extends RecyclerView.Adapter<BitacoraListaAdapter.ViewHolder> {

    private List<BitacoraLista> bitacoraLista;
    private Context context;
    private BitacoraListener mBitacoraListener;
    public BitacoraListaAdapter(List<BitacoraLista> bitacoraLista, Context context, BitacoraListener bitacoraListener ) {
        this.bitacoraLista = bitacoraLista;
        this.context = context;
        this.mBitacoraListener = bitacoraListener;
    }
    List<View>itemViewList = new ArrayList<>();
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_bitacoras, parent, false);

        return new ViewHolder(view, mBitacoraListener);
    }
    int row_index=-1;
    @Override
    public void onBindViewHolder(@NonNull BitacoraListaAdapter.ViewHolder holder, int position) {
        holder.nombreUsuario.setText(bitacoraLista.get(position).getNombreUsuario());
        holder.tituloBitacora.setText(bitacoraLista.get(position).getEtiquetasBitacora());

       /* holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=position;
                notifyDataSetChanged();
            }
        });
        if(row_index==position){
            holder.linearLayout.setBackgroundResource(R.color.item_sel);
        }
        else
        {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        }*/

    }

    @Override
    public int getItemCount() {
        return bitacoraLista.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nombreUsuario;
        TextView tituloBitacora;
        LinearLayout linearLayout;
        BitacoraListener bitacoraListener;


        public ViewHolder(@NonNull View itemView, BitacoraListener bitacoraListener) {
            super(itemView);
            nombreUsuario = itemView.findViewById(R.id.nombreUsuario);
            tituloBitacora = itemView.findViewById(R.id.tituloBitacora);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            this.bitacoraListener = bitacoraListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            bitacoraListener.onBitacoraClick(getAdapterPosition());
        }
    }
    public interface BitacoraListener{
        void onBitacoraClick(int pos);
    }


}
