package catalejoEditor;


import android.app.Activity;
import android.util.Log;
import android.webkit.JavascriptInterface;

import catalejoEditor.activities.MainActivity;

public class WebAppInterface extends Activity {

  // public static Socket socket; // Abrir sockets

  public MainActivity activity = null; // ésta variable representará la actividad principal

  // call new activity
  private static final String LOG_TAG = MainActivity.class.getSimpleName();
  public static final String EXTRA_MESSAGE = "catalejoEditor.activities.extra.MESSAGE";

  // public static final int TEXT_REQUEST = 1;

  //constructor
  public WebAppInterface(MainActivity act){
    activity = act;
    Log.d(LOG_TAG, "catalejo-msg: Construyendo WebAppInterface");
  }

  //Context mContext;

  /** Instantiate the interface and set the context */
  //WebAppInterface(Context c) {
  //    mContext = c;
  //}

  /** Show a toast from the web page */
  /*@JavascriptInterface
  public void showToast(String toast) {
      Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
  }

  @JavascriptInterface
  public void takePicture() {
      Toast.makeText(mContext, "Taking picture", Toast.LENGTH_SHORT).show();

  }*/

  /*public static void activityResult(int requestCode, int resultCode, Intent data){
    Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad");
    //right your code here .
  }*/

  /*@Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent){
    Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad");
    setExtra(intent.getExtras());
  }*/

  /** Inicia la actividad conectar con device **/
  @JavascriptInterface
  public void startConnectActivity(String code, String blocklyCode){
    Log.d(LOG_TAG, "catalejo-msg: here");
    activity.startConnectActivity(code, blocklyCode);
  }

  /** Inicia la actividad projectos **/
  @JavascriptInterface
  public void startProjectsActivity(String code, String blocklyCode){
    Log.d(LOG_TAG, "catalejo-msg: call File Activity");
    activity.startProjectsActivityFromButtonProjectWebView(code, blocklyCode);
  }

  /** Iniciar la actividad de documentación **/
  @JavascriptInterface
  public void startDocumentationActivity(String code, String blocklyCode){
    Log.d(LOG_TAG, "catalejo-msg: creación de actividad documentación");
    activity.startDocumentationActivity(code, blocklyCode);
  }

  /** Guardar cambios del proyecto en archivos **/
  @JavascriptInterface
  public void saveProject(String code, String bloclyCode){
    Log.d(LOG_TAG, "catalejo-msg: solicitud de guardado del proyecto");
    activity.saveProject(code, bloclyCode);
  }

  /** Reportar cambios en el workspace de blockly **/
  @JavascriptInterface
  public void changesDetectedInBlockly(){
    // Log.d(LOG_TAG, "catalejo-msg: reportando cambios en blockly");
    activity.reportChangesInBlockly();
  }

  /** Regresar a la actividad home guardado **/
  @JavascriptInterface
  public void returnToHome(){
    Log.d(LOG_TAG, "catalejo-msg: solicitud de regresar a la actividad home");
    activity.returnToHome();
  }

  /** Entrega el contenido del código **/
  @JavascriptInterface
  public String getCode(){
    return activity.getCodeFromData();
  }

  /** Entrega el xml de blockly **/
  @JavascriptInterface
  public String getBlocklyCode(){
    return activity.getCodeBlocklyFromData();
  }

  @JavascriptInterface
  public void setCodeBlockly(String xmlBlockly){
    activity.setCodeBlockly(xmlBlockly);
  }

}

