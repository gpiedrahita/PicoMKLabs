package catalejoEditor;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import catalejoEditor.activities.BitacoraOtrosActivity;

public class BitacoraLista extends AppCompatActivity {


    private String uId;
    private String nombreUsuario;
    private String etiquetasBitacora;
    private boolean compartida;

    public BitacoraLista(String uId, String nombreUsuario, String etiquetasBitacora, boolean compartida) {
        this.uId = uId;
        this.nombreUsuario = nombreUsuario;
        this.etiquetasBitacora = etiquetasBitacora;
        this.compartida = compartida;
    }

    public BitacoraLista(String uId) {
        this.uId = uId;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEtiquetasBitacora() { return etiquetasBitacora; }

    public void setEtiquetasBitacora(String etiquetasBitacora) {
        this.etiquetasBitacora = etiquetasBitacora;
    }

    public boolean isCompartida() {
        return compartida;
    }

    public void setCompartida(boolean compartida) {
        this.compartida = compartida;
    }

    @Override
    public String toString() {
        return "BitacoraLista{" +
                "uId='" + uId + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", tituloBitacora='" + etiquetasBitacora + '\'' +
                ", compartida=" + compartida +
                '}';
    }

}
