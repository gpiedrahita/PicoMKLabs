package catalejoEditor.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.blocklywebview.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

import catalejoEditor.BitacoraLista;
import catalejoEditor.BitacoraListaAdapter;

public class BitacoraOtrosActivity extends AppCompatActivity {
    private final FirebaseDatabase db = FirebaseDatabase.getInstance();
    DatabaseReference dbBitacoras = FirebaseDatabase.getInstance().getReference().child("users");

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitacora_otros);


        Intent intent= getIntent();
        Bundle b = intent.getExtras();
        String userName="";
        String url = "https://www.google.com/";
        File file;
        WebView myWebView = (WebView) findViewById(R.id.webViewBitacoraOtros);


        if(b!=null)
        {
            userName  =(String) b.get("usuario");
            String finalUserName = userName;
            dbBitacoras.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    for (DataSnapshot ds: snapshot.getChildren()){
                        String finaluser=ds.child("username").getValue(String.class);
                        String finalurl=ds.child("URL").getValue(String.class);
                        System.out.println("url::::"+finaluser+", "+", "+finalurl);
                        if(finaluser!=null) {
                            if (finaluser.equals(finalUserName)) {
                                myWebView.loadUrl(finalurl);
                            }
                        }
                        }
                    }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return false;
            }
        });
    }


    //Botón atras
    public void atras(View view){
        Intent session= new Intent(this, MenuOtrosActivity.class);
        startActivity(session);
    }
    //Botón salir
    public void salir(View view){
        FirebaseAuth.getInstance().signOut();
        Intent logout = new Intent(this,LoginActivity.class);
        startActivity(logout);
    }
}
