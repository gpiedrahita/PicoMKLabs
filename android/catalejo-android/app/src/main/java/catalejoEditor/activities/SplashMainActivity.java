package catalejoEditor.activities;

import android.content.Intent;

import android.os.Bundle;
import com.example.blocklywebview.R;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import catalejoEditor.services.InternalSave.saveInfo;

public class SplashMainActivity extends AppCompatActivity {

    private saveInfo saveInfo;
    private int state;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_main);
        saveInfo = new saveInfo();
        //boolean state =  saveInfo.searchVideo("validate_Key", getApplicationContext());



        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){
                if(saveInfo.searchVideo("validate_key", getApplicationContext()) == 1){
                    Intent int_init = new Intent(SplashMainActivity.this,LoginActivity.class);
                    startActivity(int_init);
                    finish();
                }else{
                    saveInfo.saveVideo(0,getApplicationContext());
                    Intent int_init2 = new Intent(SplashMainActivity.this, IntroActivity.class);
                    startActivity(int_init2);
                    finish();
                }
            }

        }, /*+*/ 2000);
    }

    public void onStart(){
        super.onStart();
        Log.w("validacionVideo", String.valueOf(saveInfo.searchVideo("validate_key", getApplicationContext())));
    }

}