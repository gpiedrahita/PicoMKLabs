package catalejoEditor.activities;

import android.animation.ObjectAnimator;
import android.content.Intent;
//import android.support.annotation.AnimatorRes;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.blocklywebview.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {
    /** Variables **/
    LinearLayout ly_login;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ly_login = (LinearLayout) findViewById(R.id.ly_login);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        session_validate();
    }


    /** FUNCION INITLOGIN
     * inicializa la actividad loginactivity2
     * parametros:
     * View: view
     * **/
    public void initLogin(View view) {
        Intent session = new Intent(this, LoginActivity2.class);
        startActivity(session);

    }
    /** FUNCION INICIO MALOKA LABS
     * Funcion que permite iniciar catalejo app
     * parametro:
     * View : view
     * **/
    public void initMalokaLabs(View view) {
        Intent sessionMaloka = new Intent(this, HomeActivity.class);
        startActivity(sessionMaloka);
    }

    /** Funcion actualizacion UI
     * Metodo que valida la session de usuario, si esta iniciada una
     * session actualiza la UI al menu principal.
     * **/
    private void session_validate(){
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            updateUI(currentUser);
        }
        else{
            Intent inte = new Intent(this, LoginActivity.class);
        }

    }

    /** Funcion actualizacion UI
     * Metodo que actualiza la UI del usuario
     * recibe parametros:
     *
     * FirebaseUser: user
     * **/
    private void updateUI(FirebaseUser user) {
        Intent intent = new Intent(this, PrincipalMenuActivity.class);
        startActivity(intent.putExtra("email", user.getEmail()));
        Toast.makeText(this, "Email User" + user.getEmail(), Toast.LENGTH_LONG).show();
       // System.out.println("Email " + user.getEmail());
        //user.getIdToken(true);
        //user.sendEmailVerification();
    }


}