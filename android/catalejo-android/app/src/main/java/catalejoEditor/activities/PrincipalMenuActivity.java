package catalejoEditor.activities;

import android.Manifest;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.blocklywebview.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import catalejoEditor.services.saveInfo.InternalSave;

public class PrincipalMenuActivity extends AppCompatActivity {

    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private VideoView videoView_intro;
    private ArrayList<String> pruebaEmail;
    InternalSave saveInfo = new InternalSave();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_menu);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        //Recibo informacion de la actividad predecesora
        /**Estructura Data recibida
         * dataUser.get(0) : email
         * dataUser.get(1) : username
         * dataUser.get(2) : tokenMoodle
         * dataUser.get(3) : Uid Firebase
         * **/

        pruebaEmail = getIntent().getStringArrayListExtra("data");
        //pruebaEmail = getIntent().getExtras().getStringArrayList("data");

        if(pruebaEmail == null){
            System.out.println("Errorrr...");
        }else{
            System.out.println(pruebaEmail.get(0));
            saveInfo.saveUid(pruebaEmail.get(3), getApplicationContext());
        }


    }

    public void initMalokaLabs(View view){
        Intent session = new Intent(this, HomeActivity.class);
        startActivity(session);
    }

    public void bitacora(View view){
        Intent session = new Intent(this, BitacoraActivity.class);
        //session.putStringArrayListExtra("data", pruebaEmail);
        //this.startActivity(session);
        startActivity(session.putExtra("data", pruebaEmail));
    }
    //Botón Atrás
    public void atras(View view){
        Intent session = new Intent(this, PrincipalMenuActivity.class);
        startActivity(session);
    }

    //Botón salir
    public void salir(View view){
            FirebaseAuth.getInstance().signOut();
            Intent logout = new Intent(this,LoginActivity.class);
            startActivity(logout);

    }

    //Botón Otros
    public void otros(View view) {
        Intent session = new Intent(this, MenuOtrosActivity.class);
        startActivity(session);
    }

    public void signOut(View view){
        FirebaseAuth.getInstance().signOut();
        Intent logout = new Intent(this,LoginActivity.class);
        startActivity(logout);

    }

    public void intro(View v){
        builder = new AlertDialog.Builder(this);
        final View video_intro = getLayoutInflater().inflate(R.layout.video_intro, null);
        videoView_intro = (VideoView) video_intro.findViewById(R.id.videoView_intro);
        builder.setView(video_intro);
        dialog = builder.create();
        dialog.show();


        videoView_intro.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.malokalabs));
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView_intro);
        videoView_intro.start();




    }
}