package catalejoEditor.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.blocklywebview.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import catalejoEditor.BitacoraLista;
import catalejoEditor.BitacoraListaAdapter;
import catalejoEditor.services.saveInfo.InternalSave;


public class MenuOtrosActivity extends AppCompatActivity {

    private final FirebaseDatabase db = FirebaseDatabase.getInstance();
    DatabaseReference dbBitacoras;
    DatabaseReference dbBitacorasCompartidas;
    private EditText buscarBitacora;
    private EditText buscarBitacoraCompartida;
    String resultado="";

    //DATOS DE USUARIO
    private String Uid;
    private ArrayList<String> data;
    InternalSave saveInfo = new InternalSave();

    //RECYCLERVIEW
    RecyclerView recyclerBitacoras;
    BitacoraListaAdapter bitacorasAdapter;
    RecyclerView recyclerBitacorasCompartidas;
    BitacoraListaAdapter bitacorasAdapterCompartidas;
    List<BitacoraLista> lista = new ArrayList<>();
    List<BitacoraLista> listaFiltro = new ArrayList<>();
    String etiquetasBitacora="";
    String nombreUsuario="";
    List<BitacoraLista> listaCompartidas = new ArrayList<>();
    List<BitacoraLista> listaFiltroCompartidas = new ArrayList<>();
    String etiquetasBitacoraCompartidas="";
    String nombreUsuarioCompartidas="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_otros);
        buscarBitacora = findViewById(R.id.filtroBitacora);
        buscarBitacoraCompartida = findViewById(R.id.filtroBitacoraCompartir);
        getIntent().getStringArrayListExtra("data");

        if (data == null) {
            Uid = saveInfo.searchUid("Uid_Key", getApplicationContext());

        } else {
            Uid = data.get(3);
        }

        inicializarElementos();
        inicializarElementosCompartidas();

        buscarBitacora.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                listaFiltro.clear();
                if(s.toString().isEmpty()){
                    recyclerBitacoras.setAdapter(new BitacoraListaAdapter(lista, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
                        @Override
                        public void onBitacoraClick(int pos) {
                            //System.out.println("lista dato::"+lista.get(pos).getNombreUsuario());
                            abrirBitacora(lista.get(pos).getNombreUsuario());
                        }
                    }));
                    bitacorasAdapter.notifyDataSetChanged();
                }
                else{
                    Filter(s.toString());
                }
            }
        });

        buscarBitacoraCompartida.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                listaFiltroCompartidas.clear();
                if(s.toString().isEmpty()){
                    recyclerBitacorasCompartidas.setAdapter(new BitacoraListaAdapter(listaCompartidas, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
                        @Override
                        public void onBitacoraClick(int pos) {
                            //System.out.println("lista dato::"+lista.get(pos).getNombreUsuario());
                            abrirBitacoraCompartida(listaCompartidas.get(pos).getNombreUsuario());
                        }
                    }));
                    bitacorasAdapterCompartidas.notifyDataSetChanged();
                }
                else{
                    FilterCompartidas(s.toString());
                }
            }
        });

    }

    private void Filter(String text) {
        for (BitacoraLista post:lista){
            etiquetasBitacora = post.getEtiquetasBitacora().toLowerCase();
            nombreUsuario = post.getNombreUsuario().toLowerCase();
            text.toLowerCase();
            if((etiquetasBitacora.contains(text))||(nombreUsuario.contains(text))){
                listaFiltro.add(post);
            }
        }

        recyclerBitacoras.setAdapter(new BitacoraListaAdapter(listaFiltro, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
            @Override
            public void onBitacoraClick(int pos) {
                //System.out.println("lista dato::"+listaFiltro.get(pos).getNombreUsuario());
                abrirBitacora(listaFiltro.get(pos).getNombreUsuario());
            }
        }));
    }
    private void FilterCompartidas(String text) {
        for (BitacoraLista post:listaCompartidas){
            etiquetasBitacoraCompartidas = post.getEtiquetasBitacora().toLowerCase();
            nombreUsuarioCompartidas = post.getNombreUsuario().toLowerCase();
            text.toLowerCase();
            if((etiquetasBitacoraCompartidas.contains(text))||(nombreUsuarioCompartidas.contains(text))){
                listaFiltroCompartidas.add(post);
            }
        }
        recyclerBitacorasCompartidas.setAdapter(new BitacoraListaAdapter(listaFiltroCompartidas, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
            @Override
            public void onBitacoraClick(int pos) {
                //System.out.println("lista dato::"+listaFiltro.get(pos).getNombreUsuario());
                abrirBitacoraCompartida(listaFiltroCompartidas.get(pos).getNombreUsuario());
            }
        }));
    }
    private void inicializarElementos () {
        recyclerBitacoras = findViewById(R.id.recycler);
        recyclerBitacoras.setHasFixedSize(true);
        recyclerBitacoras.setLayoutManager(new LinearLayoutManager(this));

        //seleccionar bitácoras
        dbBitacoras = FirebaseDatabase.getInstance().getReference().child("users");

        dbBitacoras.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds: snapshot.getChildren()){
                    nombreUsuario = ds.child("username").getValue(String.class);
                    String userKey = ds.getKey();
                    if(ds.child("Bitacora").child("etiquetas").getValue()!=null){
                        String etiquetas = ds.child("Bitacora").child("etiquetas").getValue().toString();
                        etiquetasBitacora = obtenerEtiquetas(etiquetas);
                    }else{
                        etiquetasBitacora ="";
                    }
                    lista.add(new BitacoraLista(userKey,nombreUsuario, etiquetasBitacora,false));
                    //System.out.println("restultado firebase::: "+username+":::"+userKey+":::"+ tituloBitacora);
                }
                bitacorasAdapter = new BitacoraListaAdapter(lista, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
                    @Override
                    public void onBitacoraClick(int pos) {
                        //System.out.println("lista dato::"+lista.get(pos).getNombreUsuario());
                        abrirBitacora(lista.get(pos).getNombreUsuario());
                    }
                });
                recyclerBitacoras.setAdapter(bitacorasAdapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void inicializarElementosCompartidas () {
        //compartidas
        recyclerBitacorasCompartidas = findViewById(R.id.recyclerCompartidas);
        recyclerBitacorasCompartidas.setHasFixedSize(true);
        recyclerBitacorasCompartidas.setLayoutManager(new LinearLayoutManager(this));
        //-----
        //seleccionar bitácoras
        dbBitacorasCompartidas = FirebaseDatabase.getInstance().getReference().child("users").child(Uid).child("Compartidas");

        dbBitacorasCompartidas.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds: snapshot.getChildren()){
                    nombreUsuarioCompartidas = ds.getKey();
                    etiquetasBitacoraCompartidas = ds.getValue().toString();
                    listaCompartidas.add(new BitacoraLista("",nombreUsuarioCompartidas, etiquetasBitacoraCompartidas,false));
                    //System.out.println("restultado firebase::: "+username+":::"+userKey+":::"+ tituloBitacora);
                }
                bitacorasAdapterCompartidas = new BitacoraListaAdapter(listaCompartidas, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
                    @Override
                    public void onBitacoraClick(int pos) {
                        //System.out.println("lista dato::"+lista.get(pos).getNombreUsuario());
                        abrirBitacoraCompartida(listaCompartidas.get(pos).getNombreUsuario());
                    }
                });
                recyclerBitacorasCompartidas.setAdapter(bitacorasAdapterCompartidas);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private String obtenerEtiquetas(String s) {
        String[] etiquetas = s.split(">|<");
        //System.out.println("titulo bitacora::::"+titulo[2]);
        return (etiquetas[2]);

    }

    //Botón Atrás
    public void atras(View view){
        Intent session = new Intent(this, PrincipalMenuActivity.class);
        startActivity(session);
    }

    //Botón salir
    public void salir(View view){
        FirebaseAuth.getInstance().signOut();
        Intent logout = new Intent(this,LoginActivity.class);
        startActivity(logout);
    }

    public void abrirBitacora(String user){
        System.out.println("nombre de usuario:::"+user);
        Intent intent = new Intent(this, BitacoraOtrosActivity.class);
        intent.putExtra("usuario", user);
        startActivity(intent);
    }

    public void abrirBitacoraCompartida(String user){
        //System.out.println("nombre de usuario:::"+user);
        Intent intent = new Intent(this, BitacoraActivity.class);
        intent.putExtra("usuario", user);
        //intent.putExtra("activity", "MenuOtrosActivity");
        startActivity(intent);
    }

}

