package catalejoEditor.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blocklywebview.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.annotations.NotNull;

public class passRestartActivity extends AppCompatActivity {

    private TextView email;
    private Button btn_enviar;
    private ProgressDialog Message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Message = new ProgressDialog(this);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_pass_restart);
        email = (TextView) findViewById(R.id.editTextEmail);
        btn_enviar = (Button) findViewById(R.id.btn_enviar);
        Drawable button = getResources().getDrawable(R.drawable.resource_button, getTheme());
        Drawable button_default = getResources().getDrawable(R.drawable.btn_resource_disabled, getTheme());




        email.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s.toString().equals("") && !s.toString().contains("@")) {
                    btn_enviar.setEnabled(false);
                    btn_enviar.setBackground(button_default);
                    btn_enviar.setTextColor(Color.GRAY);
                } else {
                    if(s.toString().contains("@")){
                        btn_enviar.setEnabled(true);
                        btn_enviar.setBackground(button);
                        btn_enviar.setTextColor(Color.WHITE);
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().contains("@")) {
                    btn_enviar.setEnabled(false);
                    btn_enviar.setBackground(button_default);
                    btn_enviar.setTextColor(Color.GRAY);
                }
            }


            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("") && !s.toString().contains("@")) {
                    btn_enviar.setEnabled(false);
                    btn_enviar.setBackground(button_default);
                    btn_enviar.setTextColor(Color.GRAY);
                } else {
                    if(s.toString().contains("@")){
                        btn_enviar.setEnabled(true);
                        btn_enviar.setBackground(button);
                        btn_enviar.setTextColor(Color.WHITE);
                    }

                }

            }
        });

        btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(email.getWindowToken(), 0);

                mAuth.sendPasswordResetEmail(email.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override

                    public void onComplete(@NonNull @NotNull Task<Void> task) {
                        if(task.isSuccessful()){
                            messageSummit(email.getText().toString());
                            Intent intent = new Intent(passRestartActivity.this, LoginActivity.class);
                            startActivity(intent);
                        }else{
                            Log.e("Error", task.getException().getMessage());
                            messageError(email.getText().toString());
                        }
                    }

                });
                mAuth.setLanguageCode("es");
            }
        });
    }

    public void messageSummit(String email){
        Toast.makeText(this, "correo Enviado con exito a " + email + ". verifique su correo electronico." , Toast.LENGTH_LONG).show();

    }
    public void messageError(String email){
        Toast.makeText(this, "Error... " +  "No se pudo enviar el mensaje a " + email +  ". verifique su correo electronico" , Toast.LENGTH_LONG).show();
    }
}