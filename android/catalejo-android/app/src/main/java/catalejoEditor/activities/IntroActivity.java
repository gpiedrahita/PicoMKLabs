package catalejoEditor.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;
import catalejoEditor.services.InternalSave.saveInfo;
import com.example.blocklywebview.R;



public class IntroActivity extends AppCompatActivity {

    private Button btn_saltar;
    private VideoView videoView_intro;
    private saveInfo saveInfo;
    private int state;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        btn_saltar= (Button) findViewById(R.id.btn_saltar);
        saveInfo= new saveInfo();
        btn_saltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                state = 1;
                saveInfo.saveVideo(state, getApplicationContext());
                startActivity(intent);
            }
        });
    }
    @Override
    protected  void onStart(){
      super.onStart();
        video_intro();
        state = 0;
    }


    public void video_intro(){
        videoView_intro = (VideoView) findViewById(R.id.videoView_intro);
        videoView_intro.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
        videoView_intro.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.malokalabs));
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView_intro);
        videoView_intro.setMediaController(mediaController);
        videoView_intro.start();
    }





}