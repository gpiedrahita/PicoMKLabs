    package catalejoEditor.activities;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blocklywebview.R;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.UploadTask;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import catalejoEditor.BitacoraLista;
import catalejoEditor.BitacoraListaAdapter;
import catalejoEditor.WebChromeClient;
import catalejoEditor.services.saveInfo.InternalSave;

public class BitacoraActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    //Datos de ingreso usuario
    private ArrayList<String> data;
    private AlertDialog.Builder builder;
    private boolean validate_state_bitacora;
    //Pop up opciones menú
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private EditText popup_textoBitacora;
    private RecyclerView recyclerUsuarios;
    private Button popup_btn_guardar, popup_btn_cancelar;
    private Button v_btn_aceptar, v_btn_cancelar;
    private int contador = 0;
    private int contadorAux = 0;
    // key generada para la bitácora guardada
    String key = "";
    // ruta para guardar localmente el archivo html
    String ruta = " ";

    //Variables de texto para HTML
    private String txtInput = " ";
    private String txtHTML = " ";
    private String txtHTMLFB = " ";
    private String HTMLtags = " ";
    private String HTMLtitulo = " ";
    final String HTMLopen =
            "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <meta charset=\"utf-8\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "    <title>Bitácora</title>\t\n" +
                    " <style>\n" +
                    "        button {\n" +
                    "            background-image: url('file:///android_asset/ic_delete.png');\n" +
                    "            background-size: cover;\n" +
                    "            border: none;\n" +
                    "            background-color: white;\n" +
                    "            width: 40px;\n" +
                    "            height: 40px;\n" +
                    "            font-size: 0rem;\n" +
                    "        }\n" +
                    "       #tags {\n" +
                    "            font-size: 1;\n" +
                    "               font-style: italic;\n"+
                    "       }\n"+
                    "img {\n" +
                    "max-width: 100%;\n" +
                    "max-height: 100%;\n" +
                    "}\n" +
                    "    </style>" +
                    "    </style>"+
                    "</head>\n" +
                    "<body>\n";
    final String HTMLTituloOpen =
            "    <div id=\"titulo\">\n";
    final String HTMLtituloClose =
            "    </div>\n";
    final String HTMLtagsOpen =
            "    <div id=\"tags\">\n";
    final String HTMLtagsClose =
            "    </div>\n";
    final String HTMLcontentOpen =
            "    <div id=\"content\">\n";
    final String HTMLcontentClose =
            "    </div>\n";

    final String HTMLclose =
            "<script type=\"text/javascript\">" +
                    "var idElement='';\n" +
                    "var editElem='';\n" +
                    "function editarTexto(id){\n" +
                    "editElem = document.getElementById(id);\n" +
                    " if(id==\"etiquetas\"){\n"+
                    "var idNum = id.replace(/etiquetas/, \"-1\");\n" +
                    "}else{\n"+
                    "var idNum = id.replace(/entrada/, \"\");\n" +
                    "}\n"+
                    "var edit = Android.edit();\n" +
                    "if (edit){\n" +
                    "editElem.contentEditable=\"true\";\n" +
                    "var ignoreClickOnMeElement = document.getElementById(id);\n" +
                    "\n" +
                    "document.addEventListener('click', function(event) {\n" +
                    "    var isClickInsideElement = ignoreClickOnMeElement.contains(event.target);\n" +
                    "    if (!isClickInsideElement) {\n" +
                    "        //Do something click is outside specified element\n" +
                    "        var entradaEditada= document.getElementById(id).textContent;\n"+
                    "        Android.cambioEntrada(idNum, entradaEditada);\n"+
                    "        \n" +
                    "    }\n" +
                    "});"+
                    "}\n"+
                    "}\n"+

                    "function borrarElem(id){\n" +
                    "  var idElemBorrar = parseInt(id.replace(/boton/, \"\"), 10);\n" +
                    "  idElem=id.replace(/boton/, \"entrada\")\n"+
                    "  idElemBorrar+=1;\n" +
                    "  var top = document.getElementById(\"content\");\n" +
                    "  var x = (document.getElementById(\"content\").childElementCount)/2;\n" +
                    "  var nested = document .getElementById(idElem);\n" +
                    "  var nestedButton = document.getElementById(id);\n" +
                    "  var garbage = top.removeChild(nested);\n" +
                    "  var garbageButton = top.removeChild(nestedButton);\n" +
                    "  Android.borrarEntrada(idElem);\n"+
                    " }\n"+


            "</script>"+
            "</body>\n"+
            "</html>\n";

    final String HTMLcloseFB = "</script>"+
            "</body>\n"+
            "</html>\n";

    private boolean estadoEditar=false;

    //Imagen
    private static final int PICK_IMAGE = 1;
    Uri imageUri;
    private String imageFile;
    File imgFile;

    //Firebase
    private final FirebaseDatabase db = FirebaseDatabase.getInstance();
    //private final DatabaseReference root = db.getReference().child("bitacora");
    DatabaseReference root;
    DatabaseReference rootCompartir=db.getReference("users/");
    DatabaseReference rootCompartir2=db.getReference("users/");
    HashMap<String, String> userMap = new LinkedHashMap<>();
    HashMap<String, String> userMapTemp = new LinkedHashMap<>();
    HashMap<Integer, File> imgMap = new LinkedHashMap<>();
    String etiquetasTemp;

    // request code
    private final int PICK_IMAGE_REQUEST = 22;

    // instance for firebase storage and StorageReference
    FirebaseStorage storage;
    StorageReference storageReference;
    StorageReference storageRefBitacoras;
    private Uri filePath;

    InternalSave saveInfo = new InternalSave();
    private String Uid;
    private String Uname;

    //String con nombre de usuario de bitacora compartida
    String intentString="0";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitacora);

        //Firebase  storage reference
        storage = FirebaseStorage.getInstance();

        storageReference = storage.getReference("Imagenes");
        storageRefBitacoras = storage.getReference("Bitacoras");


        // Uid de firebase
        data = getIntent().getStringArrayListExtra("data");
        //Para bitácoras compartidas (Intent de MenuOtrosActivity)
        Intent intent= getIntent();
        intentString = intent.getStringExtra("usuario");
        if (intentString!=null){
            buscarUsuarioCompartida(new FirebaseCallbackUsuario() {
                @Override
                public void onCallback(String Uidtemp) {
                    guardarUidCompartida(Uidtemp);
                }
            });

        }else{
            if (data == null) {
                Uid = saveInfo.searchUid("Uid_Key", getApplicationContext());
                Uname = saveInfo.searchUsername("Username_Key",  getApplicationContext());
                root = db.getReference("users/" + Uid);
                checkBitacora();

            } else {
                Uid = data.get(3);
                Uname = data.get(1);
                //root = db.getReference("users/" + Uid + "/Bitacora");
                root = db.getReference("users/" + Uid);
                checkBitacora();
            }
        }
    }

    protected void onStart() {
        super.onStart();
        validate_state_bitacora = true;
    }
    String finaluser="";
    private void buscarUsuarioCompartida(FirebaseCallbackUsuario FirebaseCallback){
        root = db.getReference("users/");
        root.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String uditemp="";
                for (DataSnapshot ds: snapshot.getChildren()){
                    finaluser=ds.child("username").getValue(String.class);
                    if(finaluser!=null){
                        if(finaluser.equals(intentString)){
                            uditemp=ds.getKey();
                            //System.out.println("uid usuario compartida "+uditemp);
                        }
                    }
                }
                FirebaseCallback.onCallback(uditemp);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void guardarUidCompartida(String Uidtemp){
        Uid = Uidtemp;
        Uname = intentString;
        txtHTML="";
        HTMLtitulo="";
        HTMLtags="";
        root = db.getReference("users/" + Uid);
        System.out.println("bitacora compartida de usuario: "+intentString);
        System.out.println("id usuario: "+Uid);
        checkBitacora();

    }
    public interface FirebaseCallbackUsuario {
        void onCallback(String Uidtemp);
    }

    private void checkBitacora() {
        System.out.println("id usuario checkBitacora: "+Uid);
        if (!isOnlineNet()) {
            bitacoraOffLine();
        } else {
            //root.child("Bitacora");
            root.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.child("Bitacora").getValue() == null) {
                        agregarTitulo();
                    } else {
                        bitacoraOnLine(new FirebaseCallback() {
                            @Override
                            public void onCallback(HashMap<String, String> entradas) {
                                guardarValores(entradas);
                            }
                        });
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        }
    }


    public void guardarValores(HashMap<String, String> hashmap) {

        for (Map.Entry<String, String> entry : hashmap.entrySet()) {
            if (entry.getValue().contains("<img")) {
                String iValor = entry.getKey().toString();
                iValor = iValor.split("entrada")[1];
                int i = Integer.parseInt(iValor);
                String img = encontrarImagen(entry.getValue(),i);
                txtHTML += img;
                txtHTMLFB += entry.getValue();
                userMap.put(entry.getKey(), img);
            }else if(entry.getKey().equals("entrada0")){
                HTMLtitulo=entry.getValue();
            }else if(entry.getKey().equals("etiquetas")){
                HTMLtags = entry.getValue();
            }
            else{
                txtHTML += entry.getValue();
                txtHTMLFB += entry.getValue();
            }
            userMap.put(entry.getKey(), entry.getValue());
        }
        contador = userMap.size()-1;
        guardarHtml(txtHTML);
    }

    // Abre archivo local si el dispositivo está sin conexión
    public void bitacoraOffLine() {
        File file = new File(Environment.getExternalStorageDirectory(), "/BITACORA/Bitacora" + Uid + ".html");
        if (file.exists()) {
            String temp = saveInfo.searchHTML("html_Key", getApplicationContext())[0];
            HTMLtags = saveInfo.searchHTML("html_Tags", getApplicationContext())[1];
            String cont = saveInfo.searchHTML("html_Cont", getApplicationContext())[2];
            contador = Integer.parseInt(cont);
            String[] html = temp.split("(?=<br>)");
            HTMLtitulo= html[0];
            userMap.put("entrada0", HTMLtitulo);

            for (int i = 1; i <= contador; i++) {
                if (html[i].contains("img")) {
                    userMap.put("entrada"+i, html[i]);
                    String img = encontrarImagen(html[i], i);
                    txtHTML += img;

                } else {
                    txtHTML += html[i];
                    userMap.put("entrada"+i, html[i]);
                    txtHTMLFB += html[i];
                }
            }
            userMap.put("etiquetas", HTMLtags);
            //System.out.println("searchHTML resultado::::::::" + txtHTML);
            //wvBitacora(file.toString());
            guardarHtml(txtHTML);

        } else {
            wvBitacora("file:///android_asset/test.html");
            agregarTitulo();
        }
    }

    public void bitacoraOnLine(FirebaseCallback FirebaseCallback) {

        //guardarHtml(txtHTML);

        HashMap<String, String> entradasTemp = new LinkedHashMap<>();

        //DatabaseReference uidRef = rootRef.child("ProgressReports").child(uid);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                    String entrada = dataSnapshot.child("entrada" + String.valueOf(i)).getValue(String.class);

                    if (entrada != null) {
                        entradasTemp.put("entrada" + i, entrada);
                    }
                }
                Long cont = dataSnapshot.getChildrenCount();

                String etiquetas = dataSnapshot.child("etiquetas").getValue(String.class);
                if (etiquetas != null) {
                    entradasTemp.put("etiquetas", etiquetas);
                    contador = cont.intValue()-2;
                }else {
                    contador = cont.intValue()-1;
                }
                System.out.println(".............................." + entradasTemp);
                FirebaseCallback.onCallback(entradasTemp);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        root.child("Bitacora").addListenerForSingleValueEvent(eventListener);

       /* } else {
            wvBitacora("file:///android_asset/test.html");
            agregarTitulo();
        }
*/
    }

    public interface FirebaseCallback {
        void onCallback(HashMap<String, String> entradas);
    }

    public String encontrarImagen(String url, int i) {

        int startIndex = url.lastIndexOf('/') + 1;
        int length = url.length();

        // find end index for ?
        int lastQMPos = url.lastIndexOf('?');
        if (lastQMPos == -1) {
            lastQMPos = length;
        }

        // find end index for #
        int lastHashPos = url.lastIndexOf('#');
        if (lastHashPos == -1) {
            lastHashPos = length;
        }
        String finalFileName = "";

        // calculate the end index
        int endIndex = Math.min(lastQMPos, lastHashPos);
        String fileName = url.substring(startIndex, endIndex);
        fileName = fileName.replace("Imagenes%2F", "/");
        File imgRuta = new File(Environment.getExternalStorageDirectory(), "BITACORA/IMAGENES" + fileName);
        if (imgRuta.exists()) {
            finalFileName = "<img id=\"entrada"+i+"\""+ " src=\"" +imgRuta+"\">\n<br>";
        } else {
            descargarImagen(fileName);
            finalFileName = url;
        }

        System.out.println("nombre de archivo:::" + finalFileName);
        return finalFileName;
    }

    private void descargarImagen(String fileName) {
        StorageReference imgRef = storageReference.child(fileName);

        imgRef.getBytes(1024*1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                guardarImagenLocal(bitmap, fileName);
            }
        });

    }


    // VALIDAR CONEXIÓN A INTERNET
    //----------------------------

    public Boolean isOnlineNet() {
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;

        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
          /*  if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }
*/
            if (tipoConexion1 == true || tipoConexion2 == true) {
                /* Estas conectado a internet usando wifi o redes moviles, puedes enviar tus datos */
                return true;
            }
        } else {
            /* No estas conectado a internet */
            return false;
        }
        return false;
    }


    //MENÚ BITÁCORA
    //--------------

    public void popMenu(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.popup_menu);
        popup.show();
    }

    //Botón Atrás
    public void atras(View view) {

        if (validate_state_bitacora == false) {
            validate_state_Bitacora_return(view);
        } else {
            Intent session = new Intent(this, PrincipalMenuActivity.class);
            startActivity(session);
            intentString=null;
            estadoEditar=false;
            txtHTML="";
            for (Map.Entry<String, String> entry : userMap.entrySet()) {
                if (entry.getKey().equals("entrada0")){
                    HTMLtitulo = entry.getValue();
                }else if(entry.getKey().equals("etiquetas")){
                    HTMLtags = entry.getValue();
                }else {
                    txtHTML += entry.getValue();
                }
            }
            System.out.println("html::::::"+txtHTML);
            guardarHtml(txtHTML);


        }
    }

    //Botón salir
    public void salir(View view) {
        estadoEditar=false;
        if (validate_state_bitacora == false) {
            validate_state_Bitacora_exit(view);
        } else {
            FirebaseAuth.getInstance().signOut();
            Intent logout = new Intent(this, LoginActivity.class);
            startActivity(logout);
        }
    }

    //Botón editar
    public void editar(View view) {
        popupEditar();
        txtHTML ="";
        contadorAux=0;
        imgMap.clear();
        estadoEditar=true;
       // userMapTemp.clear();
        int index = 0;
        String boton = "";

        for (Map.Entry<String, String> entry : userMap.entrySet()) {
            if(entry.getKey().equals("etiquetas")){
                HTMLtags=entry.getValue();
                userMapTemp.put(entry.getKey(), entry.getValue());
            } else if(entry.getKey().equals("entrada0")){
                HTMLtitulo=entry.getValue();
                userMapTemp.put(entry.getKey(), entry.getValue());
            }else{
                boton = "<button id=\"boton" + index + "\" type=\"button\" onclick=\"borrarElem(this.id)\">Editar2</button>\n<br>";
                userMapTemp.put(entry.getKey(), entry.getValue() + boton);
                txtHTML += entry.getValue() + boton;
            }
            index++;

        }
        guardarHtml(txtHTML);
        validate_state_bitacora = false;

    }

    public void popupEditar(){
        dialogBuilder = new AlertDialog.Builder(this);
        final View popupEditar = getLayoutInflater().inflate(R.layout.popup_editar, null);
        popup_btn_cancelar = (Button) popupEditar.findViewById(R.id.popup_btn_cancelar);

        dialogBuilder.setView(popupEditar);
        dialog = dialogBuilder.create();
        dialog.show();

        popup_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón cancelar
                dialog.dismiss();
            }
        });


    }


    //Botón guardar (GUARDA EN BASE DE DATOS)
    public void guardar(View view) {
        estadoEditar=false;
        subirImagen();
        guardarDatos();
        validate_state_bitacora = true;

    }

    public boolean guardarDatos() {
        /*key = root.push().getKey();
        assert key != null;*/
        if (!userMapTemp.isEmpty()){
            actualizarUserMap();
            System.out.println("usermap temporal no está vacío");
        }

        guardarHtml(txtHTML);
        guardarHtmlFirebase(txtHTMLFB);
       // Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));

        Uri file = Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString() + "/BITACORA/Bitacora_" + Uname + ".html"));
        StorageReference Ref = storageRefBitacoras.child("Bitacora_" + Uname + ".html");
        UploadTask uploadTask = Ref.putFile(file);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return Ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Task taskUri = root.child("URL").setValue(downloadUri.toString());

                } else {
                    // Handle failures
                    System.out.println("falló subida de archivo");
                }
            }
        });

        Task task = root.child("Bitacora").setValue(userMap);
        task.addOnSuccessListener(o -> Toast.makeText(BitacoraActivity.this, "La bitácora ha sido guardada", Toast.LENGTH_SHORT).show());
        return true;
    }

    //ITEMS MENÚ BITÁCORA
    //-------------------

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.item_texto:
                //Toast.makeText(this, "Item 1", Toast.LENGTH_SHORT).show();
                agregarTexto();
                return true;
            case R.id.item_imagen:
                agregarImagen();
                return true;
            case R.id.item_enlace:
                //Toast.makeText(this, "Item 3", Toast.LENGTH_SHORT).show();
                agregarEnlace();
                return true;
            case R.id.item_etiquetas:
                //Toast.makeText(this, "Item 3", Toast.LENGTH_SHORT).show();
                agregarEtiquetas();
                return true;
            case R.id.item_screenshot:
                screenshot();
                return true;
            case R.id.item_compartir:
                compartir();
                return true;
            default:
                return false;
        }
    }

    // DATOS DE LAS ENTRADAS DE LA BITÁCORA
    //--------------------------------------

    private void pushBitacora(String texto, Integer i) {
        if(texto.contains("<img")){
            userMap.put("entrada"+String.valueOf(i), texto);
        }else {
            userMap.put("entrada" + String.valueOf(contador), texto);
        }
        contador++;

    }

    private void actualizarUserMap() {
        userMap.clear();
        txtHTML="";
        txtHTMLFB="";
        String nuevoValor="";
        System.out.println("user map temporal actualizar::::"+ userMapTemp);
        for (Map.Entry<String, String> entry : userMapTemp.entrySet()) {
            if(entry.getKey().equals("etiquetas")){
                HTMLtags=entry.getValue();
                userMap.put("etiquetas", HTMLtags);
            } else if(entry.getKey().equals("entrada0")){
                HTMLtitulo=entry.getValue();
                userMap.put("entrada0", HTMLtitulo);
            }else {
                nuevoValor = entry.getValue().split("<button")[0];
                userMap.put(entry.getKey(), nuevoValor);
                txtHTML += nuevoValor;
                txtHTMLFB += nuevoValor;
            }

        }
        contador=userMap.size()-1;
        userMapTemp.clear();
    }


    //AGREGAR TÍTULO
    //------------------
    String titulo="";
    public void agregarTitulo() {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popupTitulo = getLayoutInflater().inflate(R.layout.popup_titulo_bitacora, null);
        popup_textoBitacora = (EditText) popupTitulo.findViewById(R.id.popup_tituloBitacora);
        popup_btn_guardar = (Button) popupTitulo.findViewById(R.id.popup_btn_guardar);
        popup_btn_cancelar = (Button) popupTitulo.findViewById(R.id.popup_btn_cancelar);

        dialogBuilder.setView(popupTitulo);
        dialog = dialogBuilder.create();
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(titulo.equals("")){
                    HTMLtitulo = "<h1 id=\"entrada0\""+" contenteditable=\"false\""+ " onclick=\"editarTexto(this.id)\">" + "Mi Bitácora" + "</h1>\n<br>";
                    userMap.put("entrada0", HTMLtitulo);
                    contador++;
                    validate_state_bitacora = false;
                }
                HTMLtags= "<p id=\"etiquetas\""+" contenteditable=\"false\""+ " onclick=\"editarTexto(this.id)\">" + "#etiquetas" + "</p>\n<br>";
                userMap.put("etiquetas", HTMLtags);
                guardarHtml(txtHTML);

            }
        });


        popup_btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón guardar
                if(!titulo.equals("")) {
                    titulo = BitacoraActivity.this.popup_textoBitacora.getText().toString();
                    txtInput = "<h1 id=\"entrada0\"" + " contenteditable=\"false\"" + " onclick=\"editarTexto(this.id)\">" + "Mi Bitácora" + "</h1>\n<br>";
                    txtHTML += txtInput;
                    pushBitacora(txtInput, 0);
                    guardarHtml(txtHTML);
                    validate_state_bitacora = false;
                }
                dialog.dismiss();
            }
        });

        popup_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón cancelar
                dialog.dismiss();
                validate_state_bitacora = true;
            }
        });
    }


    //POPUP AGREGAR TEXTO A LA BITÁCORA
    //---------------------------------

    public void agregarTexto() {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popupTexto = getLayoutInflater().inflate(R.layout.popup_texto_bitacora, null);
        popup_textoBitacora = (EditText) popupTexto.findViewById(R.id.popup_textoBitacora);
        popup_btn_guardar = (Button) popupTexto.findViewById(R.id.popup_btn_guardar);
        popup_btn_cancelar = (Button) popupTexto.findViewById(R.id.popup_btn_cancelar);

        dialogBuilder.setView(popupTexto);
        dialog = dialogBuilder.create();
        dialog.show();

        //OnClick botón de guardar - toma el texto escrito por el usuario y lo transforma a HTML
        popup_btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = BitacoraActivity.this.popup_textoBitacora.getText().toString();
                txtInput = "<p id=\"entrada"+contador+"\""+" contenteditable=\"false\"" + " onclick=\"editarTexto(this.id)\">" + Html.fromHtml(text).toString()+ "</p>\n<br>" ;
                // + "\n<button id=\"boton" + contador + "\" type=\"button\" onclick=\"editar(this.id)\">Editar</button>\n<br>"
                txtHTML += txtInput;
                pushBitacora(txtInput, 0);
                guardarHtml(txtHTML);
                validate_state_bitacora = false;
                dialog.dismiss();
            }
        });

        popup_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón cancelar
                validate_state_bitacora = true;
                dialog.dismiss();
            }
        });
    }

    //AGREGAR IMAGEN A LA BITÁCORA
    //----------------------------

    private void agregarImagen() {

        Intent gallery = new Intent();
        gallery.setType("image/*");
        gallery.setAction(Intent.ACTION_GET_CONTENT);
        validate_state_bitacora = false;
        startActivityForResult(Intent.createChooser(gallery, "Select Picture"), PICK_IMAGE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (requestCode == PICK_IMAGE && requestCode == RESULT_OK){
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                imageUri = data.getData();
                imgFile = new File(getPathFromURI(imageUri));
                contadorAux=contador;
                imgMap.put(contadorAux, imgFile);
                txtInput = "<img id=\"entrada"+contadorAux+"\""+ " src=\"" + imgFile.getPath() + "\">\n<br>";
                txtHTML += txtInput;
                contador++;
                guardarHtml(txtHTML);

                String filename = imgFile.toString().substring(imgFile.toString().lastIndexOf("/"));
                validate_state_bitacora = false;

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    guardarImagenLocal(bitmap, filename);

                } catch (IOException e) {
                    e.printStackTrace();
                    validate_state_bitacora = true;

                }
            }
        }
        // }

    }

    private void guardarImagenLocal(Bitmap bitmap, String nombreArchivo) {

        File direct = new File(Environment.getExternalStorageDirectory(), "BITACORA/IMAGENES");
        if (!direct.exists()) {
            direct.mkdirs();
            validate_state_bitacora = false;
        }
        File file = new File(direct, nombreArchivo);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            validate_state_bitacora = false;
        } catch (Exception e) {
            e.printStackTrace();
            validate_state_bitacora = true;
        }
    }

    private boolean subirImagen() {

        for (Map.Entry<Integer, File> entry : imgMap.entrySet()) {

            Uri file = Uri.fromFile(new File(entry.getValue().getPath()));
            StorageReference imgRef = storageReference.child(file.getLastPathSegment());
            UploadTask uploadTask = imgRef.putFile(file);
            validate_state_bitacora = false;

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return imgRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        txtInput = "<img id=\"entrada" +entry.getKey()+"\""+ " src=\"" + downloadUri.toString() + "\">\n<br>";
                        System.out.println("download uri ::::::::::::::: " + downloadUri);
                        pushBitacora(txtInput, entry.getKey());
                        guardarDatos();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });
        }

        return true;
    }
    @SuppressLint("ObsoleteSdkInt")
    public String getPathFromURI(Uri uri) {
        String realPath = "";
// SDK < API11
        if (Build.VERSION.SDK_INT < 11) {
            String[] proj = {MediaStore.Images.Media.DATA};
            @SuppressLint("Recycle") Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
            int column_index = 0;
            String result = "";
            if (cursor != null) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                realPath = cursor.getString(column_index);
            }
        }
        // SDK >= 11 && SDK < 19
        else if (Build.VERSION.SDK_INT < 19) {
            String[] proj = {MediaStore.Images.Media.DATA};
            CursorLoader cursorLoader = new CursorLoader(this, uri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                realPath = cursor.getString(column_index);
            }
        }
        // SDK > 19 (Android 4.4)
        else {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{id}, null);
            int columnIndex = 0;
            if (cursor != null) {
                columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    realPath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        }
        return realPath;
    }

    private String extension(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }

    //POPUP AGREGAR ENLACE A LA BITÁCORA
    //----------------------------------

    public void agregarEnlace() {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popupEnlace = getLayoutInflater().inflate(R.layout.popup_enlace_bitacora, null);
        popup_textoBitacora = (EditText) popupEnlace.findViewById(R.id.popup_enlaceBitacora);
        popup_btn_guardar = (Button) popupEnlace.findViewById(R.id.popup_btn_guardar);
        popup_btn_cancelar = (Button) popupEnlace.findViewById(R.id.popup_btn_cancelar);
        dialogBuilder.setView(popupEnlace);
        dialog = dialogBuilder.create();
        dialog.show();

        popup_btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón guardar
                String text = BitacoraActivity.this.popup_textoBitacora.getText().toString();
                txtInput = "<a id=\"entrada" + contador + "\" href=\"" + Html.fromHtml(text).toString() + "\">" + Html.fromHtml(text).toString()+ "</a>\n<br>" ;
                //+ "\n<button id=\"boton" + contador + "\" type=\"button\" onclick=\"editar(this.id)\">Editar</button>\n<br>";
                txtHTML += txtInput;
                pushBitacora(txtInput, 0);
                guardarHtml(txtHTML);
                validate_state_bitacora = false;
                dialog.dismiss();
            }
        });

        popup_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón cancelar
                validate_state_bitacora = true;
                dialog.dismiss();
            }
        });
    }

    private void agregarEtiquetas() {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popupEtiquetas = getLayoutInflater().inflate(R.layout.popup_etiquetas_bitacora, null);
        popup_textoBitacora = (EditText) popupEtiquetas.findViewById(R.id.popup_textoEtiquetas);
        popup_btn_guardar = (Button) popupEtiquetas.findViewById(R.id.popup_btn_guardar);
        popup_btn_cancelar = (Button) popupEtiquetas.findViewById(R.id.popup_btn_cancelar);

        dialogBuilder.setView(popupEtiquetas);
        dialog = dialogBuilder.create();
        dialog.show();

        popup_btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón guardar
                String text = BitacoraActivity.this.popup_textoBitacora.getText().toString();
                HTMLtags = "<p id=\"etiquetas\" contenteditable=\"false\""+ " onclick=\"editarTexto(this.id)\">" + Html.fromHtml(text).toString() + "</p>\n";
                userMap.put("etiquetas", HTMLtags);
                guardarHtml(txtHTML);
                validate_state_bitacora = false;
                dialog.dismiss();
            }
        });

        popup_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón cancelar
                validate_state_bitacora = true;
                dialog.dismiss();
            }
        });
    }

    //CAPTURA DE PANTALLA
    //-------------------

    public void screenshot() {
        View view1 = getWindow().getDecorView().getRootView();
        view1.setDrawingCacheEnabled(true);

        Bitmap bitmap = Bitmap.createBitmap(view1.getDrawingCache());
        view1.setDrawingCacheEnabled(false);

        String filePath = Environment.getExternalStorageDirectory().toString() + "/Pictures/Screenshots/" + Calendar.getInstance().getTime().toString() + ".jpg";
        File fileScreenshot = new File(filePath);

        FileOutputStream fileOutputStream = null;

        try {
            fileOutputStream = new FileOutputStream(fileScreenshot);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(fileScreenshot);
        intent.setDataAndType(uri, "image/jpeg");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);

    }

    //COMPARTIR BITÁCORA
    //------------------
    String nombreUsuario="";
    String etiquetasBitacora="";
    String nombreUsuarioCompartir="";
    String etiquetasCompartir="";
    String idUsuarioCompartir="";
    List<BitacoraLista> listaUsuarios = new ArrayList<>();
    List<BitacoraLista> listaFiltro = new ArrayList<>();
    BitacoraListaAdapter bitacorasAdapter;
    LinearLayout popup_layout;
    TextView tituloCompartir;

    private void compartir() {
        //compartir
        Resources res = getResources();
        listaUsuarios.clear();
        dialogBuilder = new AlertDialog.Builder(this);
        final View popupCompartir = getLayoutInflater().inflate(R.layout.popup_compartir_bitacora, null);
        popup_layout = (LinearLayout) popupCompartir.findViewById(R.id.layout);
        tituloCompartir = (TextView) popupCompartir.findViewById(R.id.titulo);
        String text = String.format(res.getString(R.string.compartir), "");
        tituloCompartir.setText(text);
        popup_textoBitacora = (EditText) popupCompartir.findViewById(R.id.popup_inputUsuario);
        popup_btn_guardar = (Button) popupCompartir.findViewById(R.id.popup_btn_guardar);
        popup_btn_cancelar = (Button) popupCompartir.findViewById(R.id.popup_btn_cancelar);
        recyclerUsuarios = (RecyclerView) popupCompartir.findViewById(R.id.recycler);
        recyclerUsuarios.setHasFixedSize(true);
        recyclerUsuarios.setLayoutManager(new LinearLayoutManager(this));
        dialogBuilder.setView(popupCompartir);
        dialog = dialogBuilder.create();
        dialog.show();
        inicializarElementos();

        popup_textoBitacora.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                listaFiltro.clear();
                if (s.toString().isEmpty()) {
                    recyclerUsuarios.setAdapter(new BitacoraListaAdapter(listaUsuarios, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
                        @Override
                        public void onBitacoraClick(int pos) {
                            System.out.println("lista dato::" + listaUsuarios.get(pos).getNombreUsuario());
                            idUsuarioCompartir = listaUsuarios.get(pos).getuId();
                            nombreUsuarioCompartir= listaUsuarios.get(pos).getNombreUsuario();
                            etiquetasCompartir=listaUsuarios.get(pos).getEtiquetasBitacora();
                            String text = String.format(res.getString(R.string.compartir), listaUsuarios.get(pos).getNombreUsuario());
                            tituloCompartir.setText(text);
                        }
                    }));
                    bitacorasAdapter.notifyDataSetChanged();
                } else {
                    Filter(s.toString());
                }

            }
        });

        popup_btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón guardar

                rootCompartir2 = FirebaseDatabase.getInstance().getReference().child(idUsuarioCompartir);
                etiquetasTemp=obtenerEtiquetas(HTMLtags);
                System.out.println("id usuario compartir: "+idUsuarioCompartir );
                Task task = rootCompartir.child(idUsuarioCompartir).child("Compartidas").child(Uname).setValue(etiquetasTemp);
                task.addOnSuccessListener(o -> Toast.makeText(BitacoraActivity.this, "La bitácora ha sido compartida", Toast.LENGTH_SHORT).show());
                dialog.dismiss();
            }
        });

        popup_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //definición botón cancelar
                dialog.dismiss();
            }
        });
    }


    private void inicializarElementos() {
        Resources res = getResources();
        //seleccionar Usuario
        rootCompartir = FirebaseDatabase.getInstance().getReference().child("users");

        rootCompartir.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds: snapshot.getChildren()) {
                    nombreUsuario = ds.child("username").getValue().toString();
                    String userKey = ds.getKey();
                    listaUsuarios.add(new BitacoraLista(userKey,nombreUsuario, etiquetasBitacora,false));
                }
                bitacorasAdapter = new BitacoraListaAdapter(listaUsuarios, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
                    @Override
                    public void onBitacoraClick(int pos) {
                        System.out.println("lista dato::"+listaUsuarios.get(pos).getNombreUsuario());
                        idUsuarioCompartir=listaUsuarios.get(pos).getuId();
                        nombreUsuarioCompartir= listaUsuarios.get(pos).getNombreUsuario();
                        etiquetasCompartir=listaUsuarios.get(pos).getEtiquetasBitacora();
                        String text = String.format(res.getString(R.string.compartir), listaUsuarios.get(pos).getNombreUsuario());
                        tituloCompartir.setText(text);

                    }
                });
                recyclerUsuarios.setAdapter(bitacorasAdapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
    private String obtenerEtiquetas(String s) {
        String[] etiquetas = s.split(">|<");
        //System.out.println("titulo bitacora::::"+titulo[2]);
        return (etiquetas[2]);

    }

    private void Filter(String text) {
        Resources res = getResources();

        for (BitacoraLista post:listaUsuarios){
            nombreUsuario = post.getNombreUsuario().toLowerCase();
            text.toLowerCase();
            if(nombreUsuario.contains(text)){
                listaFiltro.add(post);
            }
        }
        recyclerUsuarios.setAdapter(new BitacoraListaAdapter(listaFiltro, getApplicationContext(), new BitacoraListaAdapter.BitacoraListener() {
            @Override
            public void onBitacoraClick(int pos) {
                System.out.println("lista dato::"+listaFiltro.get(pos).getNombreUsuario());
                idUsuarioCompartir=listaFiltro.get(pos).getuId();
                nombreUsuarioCompartir= listaUsuarios.get(pos).getNombreUsuario();
                etiquetasCompartir=listaUsuarios.get(pos).getEtiquetasBitacora();
                String text = String.format(res.getString(R.string.compartir), listaFiltro.get(pos).getNombreUsuario());
                tituloCompartir.setText(text);

            }
        }));
    }

    //WEBVIEW CONTENIDO DE BITÁCORA
    //-----------------------------

    private void wvBitacora(String ruta) {
        WebView wvBitacora = (WebView) findViewById(R.id.webViewBitacora);
        wvBitacora.getSettings().setAllowContentAccess(true);
        wvBitacora.getSettings().setAllowFileAccess(true);
        WebSettings webSettings = wvBitacora.getSettings();
        wvBitacora.setWebViewClient(new WebViewClient());
        wvBitacora.setWebChromeClient(new WebChromeClient());
        wvBitacora.getSettings().setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);

        wvBitacora.loadUrl(ruta);
        wvBitacora.addJavascriptInterface(new WebAppInterface(this), "Android");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        //wvBitacora.setWebViewClient(new MyWebViewClient());

        wvBitacora.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // open in Webview
                if (url.contains("android_asset") ){
                    // Can be clever about it like so where myshost is defined in your strings file
                    // if (Uri.parse(url).getHost().equals(getString(R.string.myhost)))
                    return false;
                }
                // open rest of URLS in default browser
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
        });
    }



    // Para comunicar con Javascript
    public class WebAppInterface {
        Context mContext;
        /**
         * Instantiate the interface and set the context
         */
        WebAppInterface(Context c) {
            mContext = c;
        }
        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }
        @JavascriptInterface
        public boolean edit() {
            return (estadoEditar);
        }
        @JavascriptInterface
        public void cambioEntrada(String id, String entradaEditada) throws InterruptedException {
            txtHTML ="";
            if (id.equals("0")){
                HTMLtitulo= "<h1 id=\"entrada"+id+"\""+" contenteditable=\"false\""+ " onclick=\"editarTexto(this.id)\">"+ Html.fromHtml(entradaEditada).toString()+"</h1>\n<br>";
                userMapTemp.put("entrada"+id,HTMLtitulo);
            }else if(id.equals("-1")){
                HTMLtags = "<p id=\"etiquetas\" contenteditable=\"false\""+ " onclick=\"editarTexto(this.id)\">" + Html.fromHtml(entradaEditada).toString() + "</p>\n";
                userMapTemp.put("etiquetas",HTMLtags);
            }else{
                txtInput= "<p id=\"entrada"+id+"\""+" contenteditable=\"false\""+ " onclick=\"editarTexto(this.id)\">"+ Html.fromHtml(entradaEditada).toString()+"</p>\n<br>";
                userMapTemp.put("entrada"+id,txtInput);
            }
            System.out.println("user map temporal edicion::::::"+ userMapTemp );

            for (Map.Entry<String,String> entry : userMap.entrySet()) {
                if(!entry.getKey().equals("etiquetas")||entry.getKey().equals("entrada0")) {
                    txtHTML += entry.getValue();
                }
                /*if(entry.getKey().equals("etiquetas")){
                    HTMLtags =userMap.get("etiquetas");
                } else if(entry.getKey().equals("entrada0")){
                    HTMLtitulo =userMap.get("entrada0");
                }

                else{
                    txtHTML += entry.getValue();
                }*/
            }
        }

        @JavascriptInterface
        public void borrarEntrada(String id) {
            int idNum =Integer.parseInt(id.split("entrada")[1]);
            System.out.println("entrada borrada:::::"+id);
            Iterator it = userMapTemp.entrySet().iterator();
            String key="";
            String newKey="";
            String value="";

           /* for (Map.Entry<String, String> entry : userMapTemp.entrySet()) {

            }*/
            for (int i = idNum; i<userMapTemp.size()-1; i++){
                key = "entrada"+i;
                newKey = "entrada"+(i+1);
                value = userMapTemp.get(newKey);
                userMapTemp.put(key, value);
            }

            String borrar= "entrada"+(userMapTemp.size()-2);
            System.out.println("usermapTemporal Borrar:::::"+userMapTemp);
            userMapTemp.remove(borrar);
            String valorAnterior="";
            String valorNuevo="";
            for (int i = idNum; i<userMapTemp.size()-1; i++){
                System.out.println("iteracion:::::"+i);
                key = "entrada"+i;
                valorAnterior = userMapTemp.get(key);
                valorNuevo = valorAnterior.replace("entrada"+(i+1), "entrada"+i);
                valorNuevo = valorNuevo.replace("boton"+(i+1), "boton"+i);
                userMapTemp.put(key, valorNuevo);
            }


            System.out.println("usermap:::::::::::::::"+userMap);
            System.out.println("html:::::::::::::::"+txtHTML);
            validate_state_bitacora = false;


        }
    }

    private void guardarHtml(String html) {
        //String path = getFilesDir().getPath();
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "/BITACORA");
            //File root = new File(path, "HTML-BITACORAS");
            if (!root.exists()) {
                System.out.println("El directorio no existe");
                boolean directorio=root.mkdirs();
                if (!directorio){
                    System.out.println("Error creando el directorio");
                }else{
                    System.out.println("El directorio fuecreado");
                }
            }
            File htmlString = new File(root, "Bitacora" + Uid + ".html");
            System.out.println("file:::"+htmlString);
            ruta = Environment.getExternalStorageDirectory().toString() + "/BITACORA/Bitacora" + Uid + ".html";
            FileWriter writer = new FileWriter(htmlString, false);
            String contenido = HTMLopen+HTMLTituloOpen+HTMLtitulo+HTMLtituloClose + HTMLtagsOpen + HTMLtags + HTMLtagsClose+ HTMLcontentOpen + html + HTMLcontentClose + HTMLclose;
            writer.write(contenido);
            writer.flush();
            writer.close();
            wvBitacora(ruta);
            saveInfo.saveHTML((HTMLtitulo+html), HTMLtags, contador, getApplicationContext());
            //Toast.makeText(this, "Bitácora guardada", Toast.LENGTH_SHORT).show();
            System.out.println("guardarHtml ruta: "+ruta.toString());
            System.out.println("guardarHtml id: "+ Uid);
            System.out.println("guardarHtml name: "+ Uname);
            System.out.println("guardarHtml name: "+ html);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void guardarHtmlFirebase(String html) {

        //String path = getFilesDir().getPath();

        try {
            File root = new File(Environment.getExternalStorageDirectory(), "/BITACORA");
            //File root = new File(path, "HTML-BITACORAS");
            if (!root.exists()) {
                root.mkdirs();
            }
            File htmlString = new File(root, "Bitacora_" + Uname + ".html");
            ruta = Environment.getExternalStorageDirectory().toString() + "/BITACORA/Bitacora_" + Uname + ".html";
            System.out.println("username guardar html firebase"+Uname);
            FileWriter writer = new FileWriter(htmlString, false);
            String contenido = HTMLopen +HTMLTituloOpen+HTMLtitulo+HTMLtituloClose + HTMLtagsOpen + HTMLtags + HTMLtagsClose+  HTMLcontentOpen + html + HTMLcontentClose  + HTMLcloseFB;
            writer.write(contenido);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void validate_state_Bitacora_return(View v) {
        builder = new AlertDialog.Builder(this);

        final View bitacora_validate = getLayoutInflater().inflate(R.layout.bitacora_validate_return, null);
        v_btn_aceptar = bitacora_validate.findViewById(R.id.btn_aceptar);
        v_btn_cancelar = bitacora_validate.findViewById(R.id.btn_cancelar);
        builder.setView(bitacora_validate);
        dialog = builder.create();
        dialog.show();

        v_btn_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent session = new Intent(BitacoraActivity.this, PrincipalMenuActivity.class);
                startActivity(session);
            }
        });
        v_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

    }

    public void validate_state_Bitacora_exit(View v) {
        builder = new AlertDialog.Builder(this);

        final View bitacora_validate = getLayoutInflater().inflate(R.layout.bitacora_validate_exit, null);
        v_btn_aceptar = bitacora_validate.findViewById(R.id.btn_aceptar);
        v_btn_cancelar = bitacora_validate.findViewById(R.id.btn_cancelar);
        builder.setView(bitacora_validate);
        dialog = builder.create();
        dialog.show();
        v_btn_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent logout = new Intent(BitacoraActivity.this, LoginActivity.class);
                startActivity(logout);
            }
        });
        v_btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

    }

}