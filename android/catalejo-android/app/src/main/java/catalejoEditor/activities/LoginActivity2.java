package catalejoEditor.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blocklywebview.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.NotNull;

import java.util.ArrayList;

import catalejoEditor.services.interfaceMoodle.TokenApi;
import catalejoEditor.services.interfaceMoodle.moodleApi;
import catalejoEditor.services.models.Token;
import catalejoEditor.services.models.TokenApiRes;
import catalejoEditor.services.models.User;
import catalejoEditor.services.resourceApi.apiFunctionsMoodle;
import catalejoEditor.services.resourceApi.apiFunctionsToken;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import catalejoEditor.services.saveInfo.InternalSave;

public class LoginActivity2 extends AppCompatActivity {
    /** Text Edit **/
    private EditText userName;
    private EditText pass;
    private TextView vEmail;
    private TextView vPass;
    private TextView vPavE;
    public TextView resetPass;

    /** Firebase Variable**/
    private FirebaseAuth mAuth;

    /** ProgressBar Login**/
    private ProgressBar progressBar;

    /** API VARIABLE
     * Variable de tipo apiFunctions Moodle para el consumo del
     * Web Services.
     * **/
    private apiFunctionsMoodle apiFunctionsMoodle;
    private apiFunctionsToken apiFunctionsToken;

    User userN = new User();
    Token tokenUser = new Token();
    InternalSave saveInfo = new InternalSave();

    /**
     * username Variable
     * parametro que se usa para mantener informacion de inicio en la app
     * sin conexion.
     **/
    String usernameTemporal;
    String tokenTemporal;
    String Service1;
    String Service2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        /**Instancia de los objetos de LoginActivity**/

        mAuth = FirebaseAuth.getInstance();
        userName = (EditText) findViewById(R.id.user);
        pass = (EditText) findViewById(R.id.pass);
        vEmail = (TextView) findViewById(R.id.emailValidate);
        vPass = (TextView) findViewById(R.id.passValidate);
        vPavE = (TextView) findViewById(R.id.passandEmailValidate);

        /** Instancia variable apiFunctionsMoodle**/
        apiFunctionsMoodle = new apiFunctionsMoodle();
        /** Inicializacion de consumo de servicio por retrofit**/
        apiFunctionsToken = new apiFunctionsToken();

        //Pruebas1();
        resetPass = (TextView) findViewById(R.id.textRestartPassword);

    }

    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        session_validate();

        resetPass.setPaintFlags(resetPass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        resetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity2.this, passRestartActivity.class);
                startActivity(intent);
            }
        });

    }

    /** FUNCION INICIO DE SESION
     * Funcion que recibe una variable de tipo View instanciada por la interaccion del boton
     * que permite iniciar sesion al validar si el usuario existe en el moodle
     * **/
    public void init_Session(View view) {

        apiFunctionsToken.getRetrofit();
        apiFunctionsToken.getRetrofit2();
        apiFunctionsMoodle.getRetrofit();
        apiFunctionsMoodle.getRetrofit2();


        if (userName.length() != 0 && pass.length() != 0) {

            // tiempo de esperar para validar informacion en moodle.
            try {
                Thread.sleep(1000);
                progressBar.setVisibility(View.VISIBLE);
                vPass.setVisibility(View.INVISIBLE);
                vPavE.setVisibility(View.INVISIBLE);
                vEmail.setVisibility(View.INVISIBLE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Consulta a moodle "https://test.malokalabs.org/webservice/rest/"
            TokenApi service = apiFunctionsToken.getRetrofit().create(TokenApi.class);
            Call<TokenApiRes> tokenApiResCall = service.getTokenInfo("moodle_mobile_app", userName.getText().toString(), pass.getText().toString());
            tokenApiResCall.enqueue(new Callback<TokenApiRes>() {
                @Override
                public void onResponse(Call<TokenApiRes> call, Response<TokenApiRes> response) {
                    if (response.isSuccessful()) {
                        //Consulta a moodle "https://beta.malokalabs.org/webservice/rest/"
                        if (response.body().getToken() == null) {
                            TokenApi service = apiFunctionsToken.getRetrofit2().create(TokenApi.class);
                            Call<TokenApiRes> tokenApiResCall2 = service.getTokenInfo("moodle_mobile_app", userName.getText().toString(), pass.getText().toString());

                            tokenApiResCall2.enqueue(new Callback<TokenApiRes>() {
                                @Override
                                public void onResponse(Call<TokenApiRes> call, Response<TokenApiRes> response) {
                                    if (response.isSuccessful()) {
                                        if (response.body().getToken() == null) {
                                            Log.e("moodle_state", "No se encontro en Moodle 1");
                                            Log.e("User_state", "usuario no existe");
                                            validateLogin();
                                        } else {
                                            TokenApiRes rest = response.body();
                                            tokenUser.setToken(rest.getToken());
                                            tokenUser.setPrivatetoken(rest.getPrivatetoken());
                                            /**Si token != null
                                             * se registra o loguea en firebase
                                             * **/
                                            init_Session_App2(tokenUser.getToken());
                                            // Log.d("TOKEN", "token: " + tokenUser.getToken());
                                        }
                                    } else {
                                        Log.e("Error: ", "Error " + response.body());
                                    }
                                }

                                @Override
                                public void onFailure(Call<TokenApiRes> call, Throwable t) {
                                }
                            });
                        } else {
                            TokenApiRes rest = response.body();
                            tokenUser.setToken(rest.getToken());
                            tokenUser.setPrivatetoken(rest.getPrivatetoken());
                            /**Si token != null
                             * se registra o loguea en firebase
                             * **/
                            init_Session_App(tokenUser.getToken());
                            //Log.d("TOKEN", "token: " + tokenUser.getToken());
                        }
                    } else {
                        Log.e("Error: ", "Error " + response.body());
                    }
                }

                @Override
                public void onFailure(Call<TokenApiRes> call, Throwable t) {
                    Log.e("ErrorConexion: ", "Error" + t.getMessage());
                }
            });
            /**Instancia de consumo de servicio Web**/
        } else if (userName.length() == 0 && pass.length() != 0) {
            validateEmail();
        } else if (userName.length() != 0 && pass.length() == 0) {
            validatePass();
        } else if (userName.length() == 0 && pass.length() == 0) {
            validateEmailandPass();
        }
    }
    /** FUNCION INICIO MALOKA LABS
     * Funcion que permite iniciar catalejo app
     * parametro:
     * View : view
     * **/
    public void initMalokaLabs(View view){
        Intent session = new Intent(this, HomeActivity.class);
        startActivity(session);
    }

    /** Funcion Validacion email y pass
     * Metodo que genera la retroalimentacion adecuada al usuario
     * cuando el email o pass son incorrectos**/
    protected void validateEmailandPass() {
        vPass.setVisibility(View.VISIBLE);
        vEmail.setVisibility(View.VISIBLE);
    }
    /** Funcion Validacion email
     * Metodo que genera la retroalimentacion adecuada al usuario
     * cuando el email es incorrecto **/
    protected void validateEmail() {
        vEmail.setVisibility(View.VISIBLE);
        vPass.setVisibility(View.INVISIBLE);
    }
    /** Funcion Validacion pass
     * Metodo que genera la retroalimentacion adecuada al usuario
     * cuando el pass es incorrecto **/
    protected void validatePass() {
        vEmail.setVisibility(View.INVISIBLE);
        vPass.setVisibility(View.VISIBLE);
    }
    /** Funcion Validacion login
     * Metodo que genera la retroalimentacion adecuada al usuario
     * cuando el login no se realiza **/
    protected void validateLogin() {
        progressBar.setVisibility(View.INVISIBLE);
        vPavE.setVisibility(View.VISIBLE);

    }
    /** Funcion actualizacion UI
     * Metodo que actualiza la UI del usuario
     * recibe parametros:
     *
     * FirebaseUser: user
     * **/
    private void updateUI(FirebaseUser user, String username) {
        ArrayList<String> data = new ArrayList<>();
        Intent intent = new Intent(this, PrincipalMenuActivity.class);

        tokenTemporal = saveInfo.searchToken("tokenKey", getApplicationContext());

        if(usernameTemporal != null){
            usernameTemporal = username;
        }else{
            usernameTemporal = saveInfo.searchUsername("Username_Key" , getApplicationContext());
        }
        /*********************************************/

        data.add(user.getEmail());
        data.add(usernameTemporal);
        data.add(tokenTemporal);
        data.add(user.getUid());
        /*********************************************/

        Toast.makeText(this, "User: " + user.getEmail(), Toast.LENGTH_LONG).show();

        startActivity(intent.putExtra("data", data));

        //intent.putStringArrayListExtra("data", data);
        //startActivity(intent);

    }

    /** Funcion inicio por firebase
     * Metodo consulta e inicia sesion a partir de la informacion
     * de usuario contenida en firebase, recibe dos parametros de tipo String
     * email : String
     * password: String
     * **/
    public void session_Init_Firebase(String email, String password, String username){
        try {
            Thread.sleep(1000);
            progressBar.setVisibility(View.VISIBLE);
            vPass.setVisibility(View.INVISIBLE);
            vPavE.setVisibility(View.INVISIBLE);
            vEmail.setVisibility(View.INVISIBLE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user, username);
                    } else {
                        validateLogin();
                        Log.e("Login_state ", "Error getting sign in methods for user", task1.getException());
                    }
                });

    }


    /** Funcion actualizacion UI
     * Metodo que valida la session de usuario, si esta iniciada una
     * session actualiza la UI al menu principal.
     * **/
    private void session_validate(){
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            updateUI(currentUser, usernameTemporal);
        }
        else{
            //Intent inte = new Intent(this, LoginActivity.class);
        }

    }

    /** Funcion actualizacion UI
     * Metodo que actualiza la UI del usuario
     * recibe parametros:
     *
     * FirebaseUser: user
     * **/

    private void user_Register(String email, String password, String username, String token){
        mAuth.createUserWithEmailAndPassword(email, password)

                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("User_state", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            user.getUid();
                            insertDabatabse(user,email,username);
                            /**Actualizacion UI user**/
                            updateUI(user, userN.getUsername());
                        } else {
                            //Log.w("TAG", "LoginUser: " + email);
                            session_Init_Firebase(email, password, username);
                            // If sign in fails, display a message to the user.
                            Log.w("User_state", "Inicio sesion", task.getException());
                            Toast.makeText(LoginActivity2.this, "Inicio Sesion", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void init_Session_App(String token) {
        final String function = "core_user_get_users_by_field";
        final String key = "username";
        String value = userName.getText().toString();
        moodleApi serviceUser = apiFunctionsMoodle.getRetrofit().create(moodleApi.class);
        Call<ArrayList<User>> userApiRest = serviceUser.getUserInfo(token, function, key, value);
        userApiRest.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        Log.e("Moodle_User", "Usuario no existe");
                        // tiempo de esperar para validar informacion en moodle.
                    } else {
                        /** asignacion de data de moodle en user **/
                        ArrayList<User> UserR = response.body();
                        userN.setEmail(UserR.get(0).getEmail());
                        userN.setUsername(UserR.get(0).getUsername());
                        userN.setAuth(UserR.get(0).getAuth());
                        userN.setId(UserR.get(0).getId());
                        Log.e("Email_User", userN.getUsername());
                        /** Si emailMoodle == emailDigitado **/
                        if (userN.getUsername().equals(userName.getText().toString())) {
                            //Registra en Firebase
                            Log.e("Email_User", userN.getEmail());
                            user_Register(userN.getEmail(), pass.getText().toString(), userN.getUsername(), token);

                            /**GUARDAMOS TOKEN DE MANERA LOCAL**/
                            saveInfo.saveToken(token, getApplicationContext());
                            saveInfo.saveUsername(userN.getUsername(), getApplicationContext());


                        } else {
                            validateLogin();
                        }
                    }
                } else {

                    Log.e("ERROR_CONEXION: ", " no se puede conectar con el servidor. " + response.errorBody());
                }

            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.e("ERROR_API: ", " " + t.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }

        });

    }


    public void init_Session_App2(String token) {
        final String function = "core_user_get_users_by_field";
        final String key = "username";
        String value = userName.getText().toString();
        moodleApi serviceUser = apiFunctionsMoodle.getRetrofit2().create(moodleApi.class);
        Call<ArrayList<User>> userApiRest = serviceUser.getUserInfo(token, function, key, value);
        userApiRest.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        Log.e("Moodle_User", "Usuario no existe");
                        // tiempo de esperar para validar informacion en moodle.
                    } else {
                        /** asignacion de data de moodle en user **/
                        ArrayList<User> UserR = response.body();
                        userN.setEmail(UserR.get(0).getEmail());
                        userN.setUsername(UserR.get(0).getUsername());
                        userN.setAuth(UserR.get(0).getAuth());
                        userN.setId(UserR.get(0).getId());
                        Log.e("Email_User", userN.getUsername());
                        /** Si emailMoodle == emailDigitado **/
                        if (userN.getUsername().equals(userName.getText().toString())) {
                            //Registra en Firebase
                            Log.e("Email_User", userN.getEmail());
                            user_Register(userN.getEmail(), pass.getText().toString(), userN.getUsername(), token);

                            /**GUARDAMOS TOKEN DE MANERA LOCAL**/
                            saveInfo.saveToken(token, getApplicationContext());
                            saveInfo.saveUsername(userN.getUsername(), getApplicationContext());


                        } else {
                            validateLogin();
                        }
                    }
                } else {

                    Log.e("ERROR_CONEXION: ", " no se puede conectar con el servidor. " + response.errorBody());
                }

            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.e("ERROR_API: ", " " + t.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }

        });

    }

    public void insertDabatabse(FirebaseUser user, String email, String username ) {
        /**Insercion en RealTime Database
         * Params:
         * Firebase User
         * String email
         * String username
         * Srting token
         * **/
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users/" + user.getUid() + "/username");
        myRef.setValue(username);
        myRef = database.getReference("users/" + user.getUid() + "/email");
        myRef.setValue(email);

        //BITACORA
        /* myRef = database.getReference("users/" + user.getUid() + "/Bitacora");
        myRef.setValue("Bitacora");*/
    }

    /**FUNCION QUE PERMITE EL CAMBIO DINAMICO DEL DOMINIO MEDIANTE CONSULTA DE FIREBASE**/
  /*  public void Pruebas1(){
        FirebaseDatabase database = FirebaseDatabase.getInstance("https://clubessteam-default-rtdb.firebaseio.com");
        DatabaseReference databaseReference = database.getReference();
        databaseReference.child("Dominio_Moodle").child("Service1").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()){
                    Log.e("firebase", "Error getting data", task.getException());
                }else{
                    Service1 = String.valueOf(task.getResult().getValue());
                    Log.w("Service",  Service1);

                }
            }
        });

        databaseReference.child("Dominio_Moodle").child("Service2").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()){
                    Log.e("firebase", "Error getting data", task.getException());
                }else{
                    Service2 = String.valueOf(task.getResult().getValue());
                    Log.w("Service",  Service2);

                }
            }
        });


        databaseReference.child("Dominio_Moodle").child("Dominio1").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()){
                    Log.e("firebase", "Error getting data", task.getException());
                }else{
                    String Dominio = String.valueOf(task.getResult().getValue());
                    Log.w("test_Dominio1",  Dominio);
                    apiFunctionsMoodle.setDOMAIN_NAME(Dominio);
                }
            }
        });

        databaseReference.child("Dominio_Moodle").child("DominioToken").get(). addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()){

                }else{
                    String Dominio = String.valueOf(task.getResult().getValue());
                    apiFunctionsToken.setDOMAIN_NAME_TOKEN(Dominio);
                    Log.w("test_Dominio1", Dominio);
                }
            }
        });


        databaseReference.child("Dominio_Moodle").child("Dominio2").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()){
                    Log.e("firebase", "Error getting data", task.getException());
                }else{
                    String Dominio = String.valueOf(task.getResult().getValue());
                    Log.w("test_Dominio1",  Dominio);
                    apiFunctionsMoodle.setDOMAIN_NAME2(Dominio);
                }
            }
        });

        databaseReference.child("Dominio_Moodle").child("DominioToken2").get(). addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()){

                }else{
                    String Dominio = String.valueOf(task.getResult().getValue());
                    apiFunctionsToken.setDOMAIN_NAME_TOKEN2(Dominio);
                    Log.w("test_Dominio1", Dominio);
                }
            }
        });
    }*/

}