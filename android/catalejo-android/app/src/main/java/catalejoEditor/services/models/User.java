package catalejoEditor.services.models;;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class User {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("firstaccess")
    @Expose
    private int firstaccess;
    @SerializedName("lastaccess")
    @Expose
    private int lastaccess;
    @SerializedName("auth")
    @Expose
    private String auth;
    @SerializedName("suspended")
    @Expose
    private boolean suspended;
    @SerializedName("confirmed")
    @Expose
    private boolean confirmed;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("theme")
    @Expose
    private String theme;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("mailformat")
    @Expose
    private int mailformat;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("descriptionformat")
    @Expose
    private int descriptionformat;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("profileimageurlsmall")
    @Expose
    private String profileimageurlsmall;
    @SerializedName("profileimageurl")
    @Expose
    private String profileimageurl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getFirstaccess() {
        return firstaccess;
    }

    public void setFirstaccess(int firstaccess) {
        this.firstaccess = firstaccess;
    }

    public int getLastaccess() {
        return lastaccess;
    }

    public void setLastaccess(int lastaccess) {
        this.lastaccess = lastaccess;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public int getMailformat() {
        return mailformat;
    }

    public void setMailformat(int mailformat) {
        this.mailformat = mailformat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDescriptionformat() {
        return descriptionformat;
    }

    public void setDescriptionformat(int descriptionformat) {
        this.descriptionformat = descriptionformat;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfileimageurlsmall() {
        return profileimageurlsmall;
    }

    public void setProfileimageurlsmall(String profileimageurlsmall) {
        this.profileimageurlsmall = profileimageurlsmall;
    }

    public String getProfileimageurl() {
        return profileimageurl;
    }

    public void setProfileimageurl(String profileimageurl) {
        this.profileimageurl = profileimageurl;
    }

   /* public void userInfo(){
        System.out.println("***************** User_Info***********************");
        System.out.println("Id: " + getId());
        System.out.println("User Name: " + getUsername());
        System.out.println("full Name: " + getFullname());
        System.out.println("email: " + getEmail());
        System.out.println("***************************************************");
    }*/
}
