package catalejoEditor.services.saveInfo;

import android.app.AppComponentFactory;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;

public class InternalSave extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public  String searchToken(String token, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokenKey",0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String tokenKey = sharedPreferences.getString(token,"");
        //Log.i("VERSIONES", seccion+" "+sv);
        Log.i("Token_info ", "tokenKey " + "value recuperado: "  + tokenKey);
        return tokenKey;
    }

    public void saveToken(String token, Context context){
        System.out.println(token);
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokenKey", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("tokenKey", token);
        editor.commit();
        Log.i("Token_info ", "key " + "value: " +  token);
        //Log.i("VERSIONES", seccion+" "+sv);
    }

    public  String searchUsername(String username, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Username_Key",0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String tokenKey = sharedPreferences.getString(username,"");
        //Log.i("VERSIONES", seccion+" "+sv);
        Log.i("Token_info ", "tokenKey " + "value recuperado: "  + tokenKey);
        return tokenKey;
    }

    public void saveUsername(String username, Context context){
        System.out.println(username);
        SharedPreferences sharedPreferences = context.getSharedPreferences("Username_Key", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Username_Key", username);
        editor.commit();
        Log.i("Token_info ", "Username_Key " + "value: " +  username);
        //Log.i("VERSIONES", seccion+" "+sv);
    }

    public void saveUid(String Uid, Context context){
        System.out.println(Uid);
        SharedPreferences sharedPreferences = context.getSharedPreferences("Uid_Key", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Uid_Key", Uid);
        editor.commit();
        Log.i("Token_info ", "Username_Key " + "value: " +  Uid);
    }

    public String searchUid(String Uid, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("Uid_Key",0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String UidKey = sharedPreferences.getString(Uid,"");
        //Log.i("VERSIONES", seccion+" "+sv);
        Log.i("Token_info ", "tokenKey " + "value recuperado: "  + UidKey);
        return UidKey;
    }

    public void saveHTML(String html, String tags, int contador, Context context){
        //System.out.println(html);
        SharedPreferences sharedPreferences = context.getSharedPreferences("html_Key", 0);
        SharedPreferences sharedPreferences2 = context.getSharedPreferences("html_Tags", 0);
        SharedPreferences sharedPreferences3 = context.getSharedPreferences("html_Cont", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        SharedPreferences.Editor editor2 = sharedPreferences2.edit();
        SharedPreferences.Editor editor3 = sharedPreferences3.edit();
        editor.putString("html_Key", html);
        editor2.putString("html_Tags", tags);
        editor3.putString("html_Cont", String.valueOf(contador));
        editor.commit();
        editor2.commit();
        editor3.commit();
        //Log.i("Token_info ", "html_Cont " + "value: " +  contador);
    }

    public String[] searchHTML(String html, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("html_Key",0);
        SharedPreferences sharedPreferences2 = context.getSharedPreferences("html_Tags", 0);
        SharedPreferences sharedPreferences3 = context.getSharedPreferences("html_Cont", 0);
        String [] htmlCont = {sharedPreferences.getString(html,""),sharedPreferences2.getString(html,""),sharedPreferences3.getString(html,"")};
        //String tagCont = sharedPreferences2.getString(tags,"");
        //Log.i("VERSIONES", seccion+" "+sv);
        //Log.i("Token_info ", "htmlKey " + "value recuperado: "  + htmlCont);
        return htmlCont;
    }

}
