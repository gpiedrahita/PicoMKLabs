package catalejoEditor.services.models;;

public class TokenApiRes {

    private Token tokenApi;
    private String token;
    private String privatetoken;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPrivatetoken() {
        return privatetoken;
    }

    public Token getTokenApi() {
        return tokenApi;
    }

    public void setTokenApi(Token tokenApi) {
        this.tokenApi = tokenApi;
    }

    public void setPrivatetoken(String privatetoken) {
        this.privatetoken = privatetoken;
    }
}
