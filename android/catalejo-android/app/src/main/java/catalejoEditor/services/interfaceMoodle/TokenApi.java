package catalejoEditor.services.interfaceMoodle;;

import catalejoEditor.services.models.TokenApiRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TokenApi {

    /** CONSUMO DE SERVICIO
     * Obtiene el token del usuario registrado validado con el servicio web
     * a moodle.
     *
     * Tipo: GET
     * **/
    @GET("token.php?") //parte de la URL o funcion.
    Call<TokenApiRes> getTokenInfo(@Query("service") String service, @Query("username") String username, @Query("password") String password);
}
