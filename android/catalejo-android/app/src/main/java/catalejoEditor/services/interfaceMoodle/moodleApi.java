package catalejoEditor.services.interfaceMoodle;

import java.util.ArrayList;

import catalejoEditor.services.models.User;
import catalejoEditor.services.models.UserApiRes;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface moodleApi {

   /** CONSUMO DE SERVICIO
    * Valida por medio del servicio web de moodle el email registrado en el servidor de
    * moodle.
    *
    * Tipo: POST
    * **/
   @POST("server.php?moodlewsrestformat=json")
   Call<ArrayList<User>> getUserInfo(@Query("wstoken") String token, @Query("wsfunction") String function, @Query("field") String key, @Query("values[0]") String value );

}
