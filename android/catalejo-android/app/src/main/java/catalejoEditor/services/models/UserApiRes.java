package catalejoEditor.services.models;;

import android.util.Log;

import java.util.ArrayList;

public class UserApiRes {

    public UserApiRes(ArrayList<User> users){
        this.users = users;
    }

    public UserApiRes(){

    }


    private ArrayList<User> users;

    public ArrayList<User> getUsers() {
        return users;
    }
    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public void userView(){
        User userA = new User();

        for (int i=0; i < users.size(); i++){
            userA = users.get(i);
            userA.getEmail();
        }
        Log.i("User_info", "" + userA.getEmail());
    }
}
