package catalejoEditor.services.resourceApi;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class apiFunctionsMoodle {

    public apiFunctionsMoodle() {
        super();
    }

    /** Retrofit Variable**/
    private Retrofit retrofit;

    /** String Variable
     * Alamacena el dominio base del servicio web a consultar.
     * siempre debe finalizar '/' para ser pasada como parametro como la url
     * base del servicio.
     * **/
    private  String DOMAIN_NAME = "https://steamnacional.malokalabs.org/webservice/rest/";
    private  String DOMAIN_NAME2= "https://steambogota.malokalabs.org/webservice/rest/";


    public String getDOMAIN_NAME() {
        return DOMAIN_NAME;
    }

    public String getDOMAIN_NAME2() {
        return DOMAIN_NAME2;
    }

    public void setDOMAIN_NAME(String DOMAIN_NAME) {
        this.DOMAIN_NAME = DOMAIN_NAME;
    }

    public void setDOMAIN_NAME2(String DOMAIN_NAME2) {
        this.DOMAIN_NAME2 = DOMAIN_NAME2;
    }


    /** FUNCION GETRETROFIT
     * Tipo: Retrofit
     *
     * Devuelve un objeto retrofit donde se almacena la url base para ser consultada
     * durante el servicio web.
     * **/

    final GsonBuilder gsonBuilder = new GsonBuilder();
    final Gson gson = gsonBuilder.create();

    public Retrofit getRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder().baseUrl(getDOMAIN_NAME()).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
        return retrofit;
    }

    public Retrofit getRetrofit2() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder().baseUrl(getDOMAIN_NAME2()).addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
        return retrofit;
    }


}
