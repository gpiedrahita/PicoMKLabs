package catalejoEditor.services.resourceApi;

import android.util.Log;

import catalejoEditor.services.interfaceMoodle.TokenApi;
import catalejoEditor.services.models.Token;
import catalejoEditor.services.models.TokenApiRes;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class apiFunctionsToken {

    public apiFunctionsToken() {
        super();
    }

    /** Retrofit Variable**/
    private Retrofit retrofitToken;
    /** String Variable
     * Alamacena el dominio base del servicio web a consultar.
     * siempre debe finalizar '/' para ser pasada como parametro como la url
     * base del servicio.
     * **/
    private  String DOMAIN_NAME_TOKEN = "https://steamnacional.malokalabs.org/login/";
    private  String DOMAIN_NAME_TOKEN2 = "https://steambogota.malokalabs.org/login/";


    public String getDOMAIN_NAME_TOKEN() {
        return DOMAIN_NAME_TOKEN;
    }

    public void setDOMAIN_NAME_TOKEN(String DOMAIN_NAME_TOKEN) {
        this.DOMAIN_NAME_TOKEN = DOMAIN_NAME_TOKEN;
    }

    public String getDOMAIN_NAME_TOKEN2() {
        return DOMAIN_NAME_TOKEN2;
    }

    public void setDOMAIN_NAME_TOKEN2(String DOMAIN_NAME_TOKEN2) {
        this.DOMAIN_NAME_TOKEN2 = DOMAIN_NAME_TOKEN2;
    }

    /** FUNCION GETRETROFIT
     * Tipo: Retrofit
     *
     * Devuelve un objeto retrofit donde se almacena la url base para ser consultada
     * durante el servicio web.
     * **/
    public Retrofit getRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofitToken = new Retrofit.Builder().baseUrl(getDOMAIN_NAME_TOKEN()).addConverterFactory(GsonConverterFactory.create()).client(client).build();
        return retrofitToken;
    }

    public Retrofit getRetrofit2() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofitToken = new Retrofit.Builder().baseUrl(getDOMAIN_NAME_TOKEN2()).addConverterFactory(GsonConverterFactory.create()).client(client).build();
        return retrofitToken;
    }

}
