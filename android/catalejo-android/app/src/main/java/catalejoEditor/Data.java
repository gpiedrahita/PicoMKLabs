package catalejoEditor;
import android.os.Bundle;
import android.net.Uri;
import android.widget.TextView;

public class Data {
  public enum MessageBetweenActivities {
    FROM_HOME,
    FROM_SAVE_BLOCKLY,
    FROM_PROJECT_BLOCKLY,
    GO_TO_BLOCKLY,
    RESTART_BLOCKLY,
    RETURN,
    PASS
  }
  private String ip;
  private int port;
  private String code;
  private String blocklyCode;
  private String filename;
  private Uri uri;
  private String urlDocs;
  private MessageBetweenActivities messageBetweenActivities = MessageBetweenActivities.PASS;

  public Data(){
    this.ip = "192.168.4.1";
    this.port = 1522;
    this.code = "";
    this.blocklyCode = "<xml xmlns=\"http://www.w3.org/1999/xhtml\"></xml>";
    this.filename = "";
    this.urlDocs = "file:///android_asset/blockly/docs/index.html";
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setBlocklyCode(String blocklyCode) {
    this.blocklyCode = blocklyCode;
  }

  public void setFilename(String filename){
    this.filename = filename;
  }

  public void setUri(Uri uri){
    this.uri = uri;
  }

  public void setUrlDocs(String url){
    this.urlDocs = url;
  }

  public void setMessageBetweenActivities(MessageBetweenActivities message){
    this.messageBetweenActivities = message;
  }

  public String getIp() {
    return ip;
  }

  public int getPort() {
    return port;
  }

  public String getCode() {
    return code;
  }

  public String getBlocklyCode() {
    return blocklyCode;
  }

  public String getFilename(){
    return filename;
  }

  public Uri getUri(){
    return uri;
  }

  public String getUrlDocs(){
    return urlDocs;
  }

  public MessageBetweenActivities getMessageBetweenActivities() {
    return messageBetweenActivities;
  }

  // métodos sobrecargados
  public void setData(String code, String blocklyCode, String filename, Uri uri){
    setCode(code);
    setBlocklyCode(blocklyCode);
    setData(filename);
    setUri(uri);
  }
  
  public void setData(String code, String blocklyCode, Uri uri){
    setCode(code);
    setBlocklyCode(blocklyCode);
    setUri(uri);
  }

  public void setData(String code, String blocklyCode){
    setCode(code);
    setBlocklyCode(blocklyCode);
  }
  public void setData(String ip, int port){
    setIp(ip);
    setPort(port);
  }
  //sobre carga de operador
  public void setData(String filename){
    setFilename(filename);
  }

  public void setDataIPAndPort(String ip, int port){
    setIp(ip);
    setPort(port);
  }

  public void setDataIPAndPort(TextView ipText, TextView portText){
    setData(ipText.getText().toString(), Integer.parseInt(portText.getText().toString()));
  }

  public Bundle getExtraBundleFromData(){
    Bundle extra = new Bundle();
    extra.putString("IP", getIp());
    extra.putInt("PORT", getPort());
    extra.putString("CODE", getCode());
    extra.putString("BLOCKLYCODE", getBlocklyCode());
    extra.putString("FILENAME", getFilename());
    extra.putString("URI", (getUri() == null)?"new":getUri().toString());
    extra.putString("URL_DOCS", getUrlDocs());
    extra.putSerializable("MESSAGE_BETWEEN_ACTIVITIES", getMessageBetweenActivities());
    return extra;
  }

  public void setDataFromActivity(Bundle extra){
    setIp(extra.getString("IP"));
    setPort(extra.getInt("PORT"));
    setCode(extra.getString("CODE"));
    setBlocklyCode(extra.getString("BLOCKLYCODE"));
    setFilename(extra.getString("FILENAME"));
    setUri(Uri.parse(extra.getString("URI")));
    setUrlDocs(extra.getString("URL_DOCS"));
    setMessageBetweenActivities((MessageBetweenActivities) extra.get("MESSAGE_BETWEEN_ACTIVITIES"));
  }

}
