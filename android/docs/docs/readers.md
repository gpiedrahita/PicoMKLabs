# Lecturas

A continuación se presentan lecturas que servirán para entender las partes requeridas en el proceso de programación.

## Debug Log

[Debug log Andorid en español](https://developer.android.com/studio/debug/am-logcat?hl=es-419)

## WebView

### Refreso inesperado de WebView

[Evitar WebView se recargue debido a la rotación de la pantalla](https://stackoverflow.com/questions/44061287/android-webview-refresh-on-orientation-change?rq=1)

[Tutorial para evitar WebView se recargue debido a rotación de dispositivo](https://twigstechtips.blogspot.com/2013/08/android-retain-instance-of-webview.html)

### Ejemplo de WebView

[Usar metodos Java nativos desde WebView](http://rapidprogrammer.com/android-how-to-call-native-java-methods-from-webview-javascript)

[Repositorio de ejemplo sobre el método Java nativo desde WebView](https://github.com/spedepekka/jsjava)

## Tareas asincronas

[Documentación oficial de tareas asincronas](https://developer.android.com/reference/android/os/AsyncTask)

[Explicación sobre tareas asincronas](https://www.upwork.com/hiring/mobile/why-you-should-use-asynctask-in-android-development/)

### Parametros de distinto tipo

[Como tener distintos parámetros de entrada en una tarea asincrona](https://stackoverflow.com/questions/3921816/can-i-pass-different-types-of-parameters-to-an-asynctask-in-android)

## Sockets

[Documentación oficial para Sockets](https://developer.android.com/reference/java/net/Socket.html)

[Ejemplo de sockets en JAVA](https://www.programarya.com/Cursos-Avanzados/Java/Sockets)

[Ejemplo de socket sin tarea asincrona](http://www.androidcurso.com/index.php/recursos/43-unidad-10-internet-sockets-http-y-servicios-web/322-un-ejemplo-de-un-cliente-servidor-de-echo)

[Ejemplo de sockets pero con TCPclient y tareas asincronas](https://guides.codepath.com/android/Sending-and-Receiving-Data-with-Sockets#android-server)

[Ejemplo de TCPz-Android](https://github.com/dombrock-archive/TCPz-Android/blob/master/app/src/main/java/space/mzero/tcpz/MainActivity.java)

[Ejemplo cliente-servidor sockets](http://codigoprogramacion.com/cursos/java/103-sockets-en-java-con-cliente-y-servidor.html#.XM70Q6UnZQJ)

[Ejemplo sockets en repositorio](https://parzibyte.me/blog/2018/02/09/sockets-java-chat-cliente-servidor/)

## Activity

[Metodos de actividades](https://stackoverflow.com/questions/9723106/get-activity-instance/13326407)

### Actual Version

[Actividades con intents](https://codelabs.developers.google.com/codelabs/android-training-create-an-activity/index.html?index=..%2F..%2Fandroid-training#0)
y
[Ejemplo del repositorio](https://github.com/google-developer-training/android-fundamentals-apps-v2/tree/master/TwoActivities)

### Old Version

[Multiples actividades](https://google-developer-training.gitbooks.io/android-developer-fundamentals-course-practicals/content/en/Unit%201/21_p_create_and_start_activities.html) y
[Ejemplo de aplicación con 2 actividades](https://github.com/google-developer-training/android-fundamentals/tree/master/TwoActivities)
Recuerde que hace parte de un ejemplo del repositorio android-fundamentals

### Intent

[Intent putExtra object](https://zocada.com/using-intents-extras-pass-data-activities-android-beginners-guide/)


## Fragment in Android

[Documentación oficial de Fragment Android en español](https://developer.android.com/guide/components/fragments?hl=es-419)

## TextView in Android

[video explicativo de TextView](https://www.youtube.com/watch?v=zCi-ZEQJXlI)

[Documentaci{on oficial de TextView](https://developer.android.com/reference/android/widget/TextView)

[Ejemplo de TextView](https://abhiandroid.com/ui/textview)


## Botones en Android

[Alternativa para cambiar el color de los botones](https://stackoverflow.com/questions/3882064/how-to-change-color-of-button-in-android-when-clicked)

[Cambiar la imagen de un boton Android](https://stackoverflow.com/questions/4938480/change-button-image-in-android)

## Imagen Button en Android

[Adicionar methodos a eventos de imagenButton](https://www.mkyong.com/android/android-imagebutton-example/)

## Circular progress view

[Repositorio git](https://github.com/jakob-grabner/Circle-Progress-View)

## Imagenes e íconos

[Documentación oficial para importar íconos e imagenes vectoriales en Android Studio](https://developer.android.com/studio/write/vector-asset-studio.html)

[Importar íconos dentro de Android Studio](https://stackoverflow.com/questions/28700593/how-to-import-set-of-icons-into-android-studio-project)

[Fuente de íconos vectoriales fontawesome](https://fontawesome.com/icons?d=gallery)

[Material Desing](https://material.io/tools/icons/?search=co&style=baseline)

[Error con el color de las imágenes vectoriales](https://stackoverflow.com/questions/48207409/errorerror-fffffffff-is-incompatible-with-attribute-androidcolor-attr-co)

## File Storage

[Leer y escribir archivos con Framework de acceso a almacenamiento](https://developer.android.com/guide/topics/providers/document-provider)

[Guardar archivos en el storage](https://developer.android.com/training/data-storage/files#WriteExternalStorage)

[Obtener real path con Uri](https://freakycoder.com/android-notes-73-how-to-get-real-path-from-uri-2f78320987f5)

[Contenido de archivo a string](https://www.dev2qa.com/android-read-write-internal-storage-file-example/)

[Contenido de archivo a string old stackoverflow](https://stackoverflow.com/questions/12910503/read-file-as-string)

## Toast

Avisos emergentes

[Documentación oficial](https://developer.android.com/guide/topics/ui/notifiers/toasts#Basics)

## JSON

[Escribiendo un archivo como JSON](https://medium.com/@naimishverma50/android-writing-a-file-as-a-json-object-400131f6063b)

[Escribiendo un archivo como JSON en Java](https://crunchify.com/how-to-write-json-object-to-file-in-java/)

[JSON a String](https://geekytheory.com/trabajando-con-json-en-android)

## Pemisos

[Solicitar permisos](https://developer.android.com/training/permissions/requesting)


