function resolutionScreen(){
  return new Promise((resolve, reject)=>{
  let width = screen.width
  let height = screen.height
  //console.log(width+", "+height)
  //alert(width+", "+height)
  // verificar resolucion con el lado mas pequeño
  let pixel = width > height? height:width
  function detectmob() {
    if( navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
    ){
      return true;
    }
    else {
      return false;
    }
  }
  let size
  let device
  if (detectmob()==true) {
    screen = 'mobil'
    if (pixel > 700) {
      size = 'large'
    }else if(pixel > 450){
      size = "height: 745px; width: 595px;"
    }else{
      size = "height: 745px; width: 500px;"
    }
  }else{
    screen = 'desktop'
    size = "position: absolute;"
  }
  //console.log(screen+", "+size)
  //alert(screen+", "+size)
  return resolve({"screen":screen, "size": size, "pixels":pixel})
  })
}

var myToolbox;
var blocklyDiv;
var workspace;

resolutionScreen().then((thisScreen)=>{

// console.log(thisScreen)
//document.getElementById("blocklyDiv").style.cssText = thisScreen.size

  if (thisScreen.pixels < 459) {
  // myToolbox = toolbox2;
    myToolbox = toolboxAlt;
  }else{
    // myToolbox = toolbox1;
    myToolbox = toolbox;
  }


blocklyDiv = document.getElementById('blocklyDiv');
//blocklyDiv.style.cssText =  "width: "+screen.width+"px;"
//document.getElementById("blocklyDiv").style.cssText = "height: 745px; width: 595px;"
//console.log(document.getElementById("blocklyDiv").style)

//  var blocklyArea = document.getElementById('blocklyArea');
// console.log(blocklyArea)
//blocklyArea.style.cssText = "height: 745px; width: 595px;"
//blocklyArea.clientWidth =  745
//alert(blocklyArea.clientWidth+":"+blocklyArea.clientHeight+", "+screen.width+":"+screen.height)
  workspace = Blockly.inject(blocklyDiv,
    {media: 'media/',
  toolbox: myToolbox,
  zoom:
       {controls: true,
        wheel: true,
        startScale: 1.0,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2},
  grid:
       {spacing: 20,
    length: 3,
    colour: '#ccc',
    snap: true},
    trashcan: true
  }, {toolbox: myToolbox});
// var onresize = function(e) {
//   // Compute the absolute coordinates and dimensions of blocklyArea.
//   var element = blocklyArea;
//   var x = 0;
//   var y = 0;
//   do {
//     x += element.offsetLeft;
//     y += element.offsetTop;
//     element = element.offsetParent;
//   } while (element);
//   // Position blocklyDiv over blocklyArea.
//   blocklyDiv.style.left = x + 'px';
//   blocklyDiv.style.top = y + 'px';
//   blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
//   blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
// };
// window.addEventListener('resize', onresize, false);
// onresize();
// Blockly.svgResize(workspace);


Blockly.Xml.domToWorkspace(document.getElementById('startBlocks'),workspace);

})

/**
 * Retorna código lua
 * @function showCodeLua
 * @return {Null} null 
 */
function showCodeLua(){
  Blockly.Lua.INFINITE_LOOP_TRAP = null;
  var code = Blockly.Lua.workspaceToCode(workspace);
  document.getElementById("preview_area").innerHTML = code;
  return null
}

// Show code python
function showCodePython() {
  Blockly.Python.INFINITE_LOOP_TRAP = null;
  var code = 'import pyb\nfrom pyb import Pin\n';
  code += Blockly.Python.workspaceToCode(workspace);
  document.getElementById("preview_area").innerHTML = code;
}

// Show code [lua, python]
function showCode() {
  showCodeLua();
  // showCodePython();
}

/**
 * Permite activar la actividad referente a la conexión con el dispositivo
 * @function connect
 * @return {Null} Null 
 */
function connect(){
  // Android.startConnectActivity("", getXMLBlockly())  
  Android.startConnectActivity(getCodeLua(), getXMLBlockly())  
  // Android.startConnectActivity(getCodePython(), getXMLBlockly())  
}

/**
* Permite activar la actividad referenye a el file system
* @function file
* @return {Null} null 
*/
function file(){
  // Android.startFileActivity("", getXMLBlockly())  
  Android.startFileActivity(getCodeLua(), getXMLBlockly())  
  // Android.startFileActivity(getCodePython(), getXMLBlockly())  
}

function docs(){
  Android.startDocumentationActivity(getCodeLua(), getXMLBlockly())  
}

/**
 * Captura código Lua desde blockly
 * @function getCodeLua
 * @return {String} code codigo Lua
 */
function getCodeLua(){
  Blockly.Lua.INFINITE_LOOP_TRAP = null
  var code = Blockly.Lua.workspaceToCode(workspace)
  return code
}

/**
 * Captura de código Python desde blockly
 * @function getCodePython
 * @return {String} code código Python
 */
function getCodePython(){
  Blockly.Python.INFINITE_LOOP_TRAP = null
  var code = 'import pyb\npyb.LED(1).on()\n\x05\n\nfrom pyb import Pin\npyb.LED(1).off()\n';
  code += Blockly.Python.workspaceToCode(workspace)
  code += '\x04\n';
  return code
}

/**
 * Obtener XML Blockly
 * @function getXMLBlockly
 * @return {String} xmlBlockly Representación del xml de blockly
 */
function getXMLBlockly(){
  var xml = Blockly.Xml.workspaceToDom(workspace);
  var xmlBlockly = Blockly.Xml.domToText(xml);
  return xmlBlockly
}

/**
 * Escribe el workspace de blockly a partir de un proyecto previamente almacenado en la actividad principal
 * @function setWorkspaceBlockly
 * @param {String} textXmlBlockly representación en string del proyecto (blockly)
 * @return {Null} null
 */
function setWorkspaceBlockly(textXmlBlockly){ 
  var xml = Blockly.Xml.textToDom(textXmlBlockly);
  if (xml != "") {
    workspace.clear()
    Blockly.Xml.domToWorkspace(xml, workspace);
  }
}

/**************************************/

/**
 * hacer grabación temporal del código blockly
 * @function snap
 * @return {Null} Null 
 */
function snap(){
  Android.setCodeBlockly(getXMLBlockly())  
}

/**
 * Actualiza el area de trabajo de blockly con una copia temporal previamente almacenada en la actividad principal
 * @function loadSnap
 * @return {Null} null
 */
function loadSnap(){
  // loadBlocksFromfile es una variable auxiliar PROVICIONAL que requiere otra lógica. se hace con el fin de no borrar trabajos anteriores
  blocksFromFile = Android.getBlocklyCode()
  if(blocksFromFile != "<xml xmlns=\"http://www.w3.org/1999/xhtml\"></xml>"){
    setWorkspaceBlockly(Android.getBlocklyCode())
  }
}
/**************************************/

/** OLD CODE **/

//Run code python
function runCodePython(){
  Blockly.Python.INFINITE_LOOP_TRAP = null;
    var code = 'import pyb\npyb.LED(1).on()\n\x05\n\nfrom pyb import Pin\npyb.LED(1).off()\n';
    code += Blockly.Python.workspaceToCode(workspace);
  code += '\x04\n';
  enviar(code);
}

//Save code python
function saveCodePython(){
  Blockly.Python.INFINITE_LOOP_TRAP = null;
    var code = 'import pyb\npyb.LED(1).on()\n\x05\n\npyb.delay(1000)\nfrom pyb import Pin\npyb.LED(1).off()\n';
    code += Blockly.Python.workspaceToCode(workspace);
  code += '\x04\n';
  var codetmp = code.split('\n');
  code = "";
  for (var i = 0, l = codetmp.length; i < l; i++) {
    code += "save__data"+codetmp[i]+'\n';
  }
  //console.log("save__begin\n"+code+"save__end\n");
  enviar("save__begin\n"+code+"save__end\n");
}

//Remove code python
function removeCodePython(){
  enviar("save__rm\n");
}

function importBlocks(){
  var x = document.getElementById("openFile");
  var reader = new FileReader();

  reader.onload = function(e) {
  var xml_text = reader.result;
  var xml = Blockly.Xml.textToDom(xml_text);
  Blockly.Xml.domToWorkspace(xml, workspace);
  //console.log(xml);
  }
  reader.readAsText(x.files[0]);
}

// Save Blocks xml
var nameFileproyect = "";
function exportBlocks(){
  if (nameFileproyect == null){
    nameFileproyect = "uname";
  }
  nameFileproyect = prompt("Nombre del Proyecto", nameFileproyect);
  if(nameFileproyect != null && nameFileproyect != ""){
    var xml = Blockly.Xml.workspaceToDom(workspace);
    var xml_text = Blockly.Xml.domToText(xml);
    var blob = new Blob([xml_text], {type: "text/plain;charset=utf-8"});
    saveAs(blob, nameFileproyect+".xml");
  }
}

//*****	BEGIN WEBSOCKET ******//
// Connect2medialab
var mysocket;
var card_ip = "192.168.4.1";

function connect2medialab(){
  if (card_ip == null){
      card_ip = "192.168.4.1";
  }
    var cardIP = prompt("Ingrese la dirección IP de la tarjeta a programar", card_ip);
  if (cardIP != null && cardIP != ""){
    card_ip = cardIP;
    mysocket = new WebSocket(`ws://${cardIP}:3335`);
    waitForSocketConnection(mysocket, webSocketHandler("\r\n"));
  }
    else if (cardIP == ""){
    cardIP = card_ip;
    alert("Dirección IP inválida.");
  }
  else if (cardIP == null){
    cardIP = card_ip;
    alert("Comunicación cancelada.");
  }
  console.log(`ip:${cardIP}`);
}

// DisconnectMedialab
function disconnectWebsocket(){
    mysocket.close();
}

// Connection WebSocket
function waitForSocketConnection(socket, callback) {
  setTimeout(
    function () {
      if (socket.readyState == 1) {
        console.log("Connection is made")
    alert("Conexión establecida.");
        if(callback != null){
          callback();
        }
        return;
      } else {
        console.log("Connecting timeOut.")
    alert("Se excedió el tiempo de espera.");
      }
  }, 4000); // wait 4000 milisecond for the connection...
}

function webSocketHandler(onMessageMsg) {
  mysocket.onopen = function() {
    console.log('Connection opened.');  
  alert("Abriendo conexión...");
    connectionState = 1;
    //mysocket.send(onMessageMsg);
  }
  
  mysocket.onclose = function(e) {
  console.log("WebSocket Closed.");
  alert("Conexión cerrada.");
    connectionState = 0;
  }

  mysocket.onmessage = function(e) {
  document.getElementById("areaReceive").value =
  e.data + document.getElementById("areaReceive").value;
  lineCounter = lineCounter + 1;
  if(lineCounter < lineas.length){
    mysocket.send(lineas[lineCounter]+"\r");
    document.getElementById("areaSend").value =
    document.getElementById("areaSend").value + lineas[lineCounter] + "\r\n";
    if (lineCounter == (lineas.length-1)){
      alert("Programa enviado a Media_Lab y ejecutandose");
    }
  } else if(e.data.search("MicroPython")>-1){
    lineCounter = 0;
    lineas = [];
    document.getElementById("areaReceive").value = e.data;
    alert("Reset en tarjeta Media_Lab")
  }
    else if(lineCounter == lineas.length){
    lineCounter = 0;
    lineas = [];
    alert("Ejecución en Media_Lab terminada");
  }
  }
}

// Start Send Code
var lineCounter = 0;
var lineas = [];

function enviar(code){	
  lineCounter = 0;
  lineas = [];
  document.getElementById("areaSend").value = "";
  document.getElementById("areaReceive").value = "";
  lineas = code.split('\n');
  //console.log(lineas);
  mysocket.send(lineas[lineCounter]+"\r");
  document.getElementById("areaSend").value =
  document.getElementById("areaSend").value + lineas[lineCounter] + "\r\n";	
}
// End send code
//*****	END WEBSOCKET ******//

