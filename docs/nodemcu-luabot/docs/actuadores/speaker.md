# Parlante

* El sonido puede ser generado por hardware con una frecuencia entre 0 Hz hasta 1000 Hertz
* Si se desea obtener frecuencias superiores se podría estables por software, sin embargo no es recomendable
* El volumen del sonido puede ser logrado variando el ciclo útil entre 0 a 512.

## Ejemplos

### Sonido ON/OFF

En este ejemplo se generará un sonido a una frecuencia y volumen fijo, notaremos como en cada segundo que pase
se presentará un sonido y luego un silencio; esto se repetirá por 4 veces para luego quedar en silencio.

#### Conexiones

![parlante pictografico](../img/actuadores/parlante/nodemcu_parlante_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

#### Algoritmo

![parlante algoritmo](../img/actuadores/parlante/parlante-algoritmo.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}


